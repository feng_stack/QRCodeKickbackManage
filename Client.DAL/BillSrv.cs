﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using Models;
using Client.Common;

namespace Client.DAL
{
    public static class BillSrv
    {
        #region 公共

        public static void GetCodeInfo(Action<RetModel> callback, string fullCode)
        {
            var dc = Comm.GetDC();
            dc["codeFull"] = fullCode;
            dc["cmd"] = Cmd.Bill_GetCodeInfo;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 公共

        #region 销售

        #region 增加销售记录

        public static void AddSaleBill(Action<RetModel> callback, CustomModel cm, List<BillEntryModel> bem)
        {
            var dc = Comm.GetDC();
            dc["custInfo"] = cm;
            dc["entry"] = bem;
            dc["cmd"] = Cmd.Bill_AddSaleBill;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 增加销售记录

        #endregion 销售

        #region 兑换

        // 增加兑换记录
        public static void AddChargeBill(Action<RetModel> callback, CustomModel cm, List<BillEntryModel> bem)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Bill_AddChargeBill;
            dc["custInfo"] = cm;
            dc["entry"] = bem;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 兑换

        #region 报表

        //产品汇总表
        public static void GetProSummary(Action<RetModel> callback, string sDate, string eDate, int proId = 0)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Bill_GetProSummary;
            dc["sDate"] = sDate;
            dc["eDate"] = eDate;
            dc["proId"] = proId;
            Comm.NetCtrl.Send(dc, callback);
        }

        //兑换汇总表
        public static void GetChargeSummary(Action<RetModel> callback, string sDate, string eDate, int proId = 0, int custCate = 0, int custId = 0)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Bill_GetChargeSummary;
            dc["sDate"] = sDate;
            dc["eDate"] = eDate;
            dc["proId"] = proId;
            dc["cateId"] = custCate;
            dc["custId"] = custId;
            Comm.NetCtrl.Send(dc, callback);
        }

        //销售汇总表
        public static void GetSaleSummary(Action<RetModel> callback, string sDate, string eDate, int custCate = 0, int custId = 0, int proId = 0)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Bill_GetSaleSummary;
            dc["sDate"] = sDate;
            dc["eDate"] = eDate;
            dc["cateId"] = custCate;
            dc["custId"] = custId;
            dc["proId"] = proId;
            Comm.NetCtrl.Send(dc, callback);
        }

        //销售明细
        public static void GetSaleDetail(Action<RetModel> callback, string sDate, string eDate, int proId = 0, int custCate = 0, int custId = 0)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Bill_GetSaleDetail;
            dc["sDate"] = sDate;
            dc["eDate"] = eDate;
            dc["proId"] = proId;
            dc["cateId"] = custCate;
            dc["custId"] = custId;
            Comm.NetCtrl.Send(dc, callback);
        }

        //兑换明细
        public static void GetChargeDetail(Action<RetModel> callback, string sDate, string eDate, int proId = 0, int custCate = 0, int custId = 0)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Bill_GetChargeDetail;
            dc["sDate"] = sDate;
            dc["eDate"] = eDate;
            dc["proId"] = proId;
            dc["cateId"] = custCate;
            dc["custId"] = custId;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 报表

        #region 积分单

        //读积分兑换单
        public static void GetPointBill(Action<RetModel> callback, int bId)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Bill_GetPointBill;
            dc["bId"] = bId;
            Comm.NetCtrl.Send(dc, callback);
        }

        //读积分单列表
        public static void GetPointBillList(Action<RetModel> callback, string sDate, string eDate, string custName, int maker)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Bill_GetPointBillList;
            dc[nameof(sDate)] = sDate;
            dc[nameof(eDate)] = eDate;
            dc[nameof(custName)] = custName;
            dc[nameof(maker)] = maker;
            Comm.NetCtrl.Send(dc, callback);
        }

        //保存积分使用单
        public static void SavePointBill(Action<RetModel> callback, PointBillModel pm)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Bill_SavePointBill;
            dc[nameof(pm)] = pm;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 积分单
    }
}