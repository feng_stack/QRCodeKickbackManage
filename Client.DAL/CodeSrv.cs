﻿using System;
using System.Collections.Generic;

using Client.Common;

using Models;

namespace Client.DAL
{
    public static class CodeSrv
    {
        #region 增加一个码

        //单个增加
        public static void AddCode(Action<RetModel> callback, CodeModel cm)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Code_AddCode;
            dc["codeInfo"] = cm;
            Comm.NetCtrl.Send(dc, callback);
        }

        //批量增加
        public static void AddCodeBatch(Action<RetModel> callback, List<CodeModel> codeList)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Code_AddCodeBatch;
            dc["codeList"] = codeList;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 增加一个码

        #region 读取码列表

        public static void GetCodeList(Action<RetModel> callback, string year, string suffix)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Code_GetCodeList;
            dc["year"] = year;
            dc["suffix"] = suffix;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 读取码列表

        #region 读码状态列表

        public static void GetStatusList(Action<RetModel> callback, int proId, string sDate, string eDate, byte status = 255)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Code_GetStatusList;
            dc["proId"] = proId;
            dc["sDate"] = sDate;
            dc["eDate"] = eDate;
            dc["state"] = status;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 读码状态列表

        #region 空码删除

        public static void CancelCode(Action<RetModel> callback, int codeId)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Code_CancelCode;
            dc["codeId"] = codeId;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 空码删除
    }
}