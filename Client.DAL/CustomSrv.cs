﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data.SQLite;
using Client.Common;
using System.Data;

namespace Client.DAL
{
    public static class CustomSrv
    {
        #region 类别

        #region 增加类别

        public static void AddCate(Action<RetModel> callback, CustomCateModel cm)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_Cate_AddCate;
            dc["cateInfo"] = cm;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 增加类别

        #region 修改类别

        public static void EditCate(Action<RetModel> callback, CustomCateModel cm)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_Cate_EditCate;
            dc["cateInfo"] = cm;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 修改类别

        #region 删除类别

        public static void DelCate(Action<RetModel> callback, int cateId)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_Cate_DelCate;
            dc["cateId"] = cateId;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 删除类别

        #region 读取类别信息

        public static void GetCateInfo(Action<RetModel> callback, int cateId = 0, string cateCode = "")
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_Cate_GetCateInfo;
            dc["cateId"] = cateId;
            dc["cateCode"] = cateCode;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 读取类别信息

        #region 读类别列表

        public static void GetCateList(Action<RetModel> callback)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_Cate_GetCateList;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 读类别列表

        #region 获取类别下的客户数量

        public static void GetCateChildCount(Action<RetModel> callback, int cateId)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_Cate_GetCateChildCount;
            dc["cateId"] = cateId;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 获取类别下的客户数量

        #endregion 类别

        #region 客户

        #region 增加客户

        public static void AddCust(Action<RetModel> callback, CustomModel cm)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_AddCust;
            dc["custInfo"] = cm;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 增加客户

        #region 修改客户

        public static void EditCust(Action<RetModel> callback, CustomModel cm)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_EditCust;
            dc["custInfo"] = cm;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 修改客户

        #region 启用客户

        public static void EnableCust(Action<RetModel> callback, int custId)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_EnableCust;
            dc["custId"] = custId;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 启用客户

        #region 禁用客户

        public static void DisableCust(Action<RetModel> callback, int custId)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_DisableCust;
            dc["custId"] = custId;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 禁用客户

        #region 读客户信息

        public static void GetCustInfo(Action<RetModel> callback, int custId = 0, string custCode = "")
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_GetCustInfo;
            dc["custId"] = custId;
            dc["custCode"] = custCode;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 读客户信息

        #region 更新用户兑换汇总

        public static void UpdateCustSummary(Action<RetModel> callback, int uId)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_UpdateCustSummary;
            dc["userId"] = uId;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 更新用户兑换汇总

        #region 读客户列表

        public static void GetCustList(Action<RetModel> callback, int cateId = 0, byte state = 0)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Cust_GetCustList;
            dc["cateId"] = cateId;
            dc["state"] = state;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 读客户列表

        #endregion 客户
    }
}