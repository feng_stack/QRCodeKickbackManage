﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data;
using System.Data.SQLite;
using Client.Common;

namespace Client.DAL
{
    public static class ProSrv
    {
        #region 增加一个商品

        public static void AddProduct(Action<RetModel> callback, ProductModel p)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Pro_AddProduct;
            dc["proInfo"] = p;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 增加一个商品

        #region 读取单个商品信息

        public static void GetProInfo(Action<RetModel> callback, int proId = 0, string proCode = "")
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Pro_GetProInfo;
            dc["proId"] = proId;
            dc["proCode"] = proCode;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 读取单个商品信息

        #region 读产品列表

        public static void GetProList(Action<RetModel> callback, string proCode = "", string proName = "", byte proState = 255)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Pro_GetProList;
            dc["proCode"] = proCode;
            dc["proName"] = proName;
            dc["state"] = proState;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 读产品列表

        #region 启用/禁用

        public static void DisablePro(Action<RetModel> callback, int proId)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Pro_DisablePro;
            dc["proId"] = proId;
            Comm.NetCtrl.Send(dc, callback);
        }

        public static void EnablePro(Action<RetModel> callback, int proId)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Pro_EnablePro;
            dc["proId"] = proId;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 启用/禁用

        #region 更新兑换价

        public static void UpdatePrice(Action<RetModel> callback, int proId, decimal price)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Pro_UpdatePrice;
            dc["proId"] = proId;
            dc["price"] = price;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 更新兑换价

        #region 修改产品

        public static void UpdateProduct(Action<RetModel> callback, ProductModel pro)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Pro_EditPro;
            dc["proInfo"] = pro;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 修改产品
    }
}