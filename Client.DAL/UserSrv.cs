﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data;
using System.Data.SQLite;
using Client.Common;

namespace Client.DAL
{
    public static class UserSrv
    {
        #region 读用户信息

        public static void GetUserInfo(Action<RetModel> callback, string userName)
        {
            var dc = Comm.GetDC();
            dc["userName"] = userName;
            dc["cmd"] = Cmd.Usr_GetUserInfo;
            Comm.NetCtrl.Send(dc, callback);
        }

        public static void GetUserInfo(Action<RetModel> callback, int uId)
        {
            var dc = Comm.GetDC();
            dc["userId"] = uId;
            dc["cmd"] = Cmd.Usr_GetUserInfo;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 读用户信息

        #region 读用户列表

        public static void GetUserList(Action<RetModel> callback, byte state = 0)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Usr_GetUserList;
            dc["state"] = 0;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 读用户列表

        #region 增加用户

        public static void AddUser(Action<RetModel> callback, UserModel um)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Usr_AddUser;
            dc["userInfo"] = um;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 增加用户

        #region 修改密码

        public static void EditPass(Action<RetModel> callback, string oldPass, string newPass, int userId)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Usr_EditPass;
            dc["oldPass"] = oldPass;
            dc["newPass"] = newPass;
            dc["userId"] = userId;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 修改密码

        #region 修改用户信息

        public static void EditUser(Action<RetModel> callback, UserModel um)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Usr_EditUser;
            dc["userInfo"] = um;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 修改用户信息

        #region 修改用户状态

        public static void EnableUser(Action<RetModel> callback, int uId)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Usr_EnableUser;
            dc["userId"] = uId;
            Comm.NetCtrl.Send(dc, callback);
        }

        public static void DisableUser(Action<RetModel> callback, int uId)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Usr_DisableUser;
            dc["userId"] = uId;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 修改用户状态

        #region 系统登录

        public static void Login(Action<RetModel> callback, UserModel um)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Usr_Login;
            dc["userInfo"] = um;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 系统登录

        #region 心跳

        public static void HeartBeat(Action<RetModel> callback)
        {
            var dc = Comm.GetDC();
            dc["cmd"] = Cmd.Usr_HeartBeat;
            Comm.NetCtrl.Send(dc, callback);
        }

        #endregion 心跳
    }
}