﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Client.Common
{
    public class Events
    {
        public delegate void InvokeHandler();

        public delegate void RecvHandler(RetModel rm);

        public event InvokeHandler InvokeEvent;

        public event RecvHandler RecvEvent;

        public void RaiseInvoke()
        {
            InvokeEvent?.Invoke();
        }

        public void RaiseRecv(RetModel rm)
        {
            RecvEvent?.Invoke(rm);
        }
    }
}