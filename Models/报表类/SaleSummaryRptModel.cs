﻿namespace Models
{
    /// <summary>
    /// 客户销售统计汇总表
    /// </summary>
    public class SaleSummaryRptModel
    {
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public string ProCode { get; set; }
        public string ProName { get; set; }
        public decimal SaleCount { get; set; }
    }
}