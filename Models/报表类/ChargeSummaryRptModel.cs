﻿namespace Models
{
    /// <summary>
    /// 客户兑换汇总统计表
    /// </summary>
    public class ChargeSummaryRptModel
    {
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public string ProCode { get; set; }
        public string ProName { get; set; }
        public decimal ChargeCount { get; set; }
        public decimal ChargeAmount { get; set; }
    }
}