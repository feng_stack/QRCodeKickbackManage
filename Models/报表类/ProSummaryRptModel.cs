﻿namespace Models
{
    /// <summary>
    /// 产品汇总表
    /// </summary>
    public class ProSummaryRptModel
    {
        public string ProCode { get; set; }
        public string ProName { get; set; }
        public decimal SaleCount { get; set; }
        public decimal ChargeCount { get; set; }
        public decimal ChargeAmount { get; set; }
    }
}