﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class PointBillModel : ModelBase
    {
        public int Id { get; set; }
        public int CustId { get; set; }
        public string CustName { get; set; }
        public string CustPhone { get; set; }
        public string BillDate { get; set; }
        public int UsePoint { get; set; }
        public string Remark { get; set; }
        public int MakerId { get; set; }
        public string MakerName { get; set; }
    }
}