﻿namespace Models
{
    public class CodeModel
    {
        public int Id { get; set; }
        public int ProId { get; set; }
        public string CodeFull { get; set; }
        public string CodePro { get; set; }
        public string CodeYear { get; set; }
        public int CodeNum { get; set; }
        public string CodeMain { get; set; }
        public string CodeSuffix { get; set; }
        public string GenDate { get; set; }
        public int OperatorId { get; set; }
        public byte Status { get; set; } //0正常1作废
    }
}