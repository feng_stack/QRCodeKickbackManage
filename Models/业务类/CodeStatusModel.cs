﻿namespace Models
{
    public class CodeStatusModel
    {
        public CodeModel CodeBaseInfo { get; set; }
        public int CodeId { get; set; }
        public byte SaleFlag { get; set; }
        public int SaleCustId { get; set; }
        public string SaleDate { get; set; }
        public int SaleMaker { get; set; }
        public byte ChargeFlag { get; set; }
        public int ChargerId { get; set; }
        public string ChargeDate { get; set; }
        public int ChargeMaker { get; set; }
        public decimal ChargeAmount { get; set; }
    }
}