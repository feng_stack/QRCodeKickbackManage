﻿namespace Models
{
    public class BillEntryModel : ModelBase
    {
        #region 产品

        public int ProId { get; set; } //产品ID
        public string ProCode { get; set; } //产品代码
        public string ProName { get; set; }
        public string ProModel { get; set; }
        public string ProRemark { get; set; }
        public decimal ProDefaultPrice { get; set; }

        #endregion 产品

        #region 编码

        public int CodeId { get; set; } //code_id
        public string CodeFull { get; set; } //条码
        public string CodeYear { get; set; }
        public string CodeMain { get; set; }
        public int CodeNum { get; set; }
        public string CodeSuffix { get; set; }
        public string CodeGenDate { get; set; }
        public int CodeMakerId { get; set; }
        public string CodeMakerUser { get; set; }
        public string CodeMakerName { get; set; }
        public byte CodeStatus { get; set; }

        #endregion 编码

        //失败原因
        public string ErrReason { get; set; }

        #region 销售信息

        public byte SaleFlag { get; set; }
        public string SaleFlagString { get; set; }
        public string SaleDate { get; set; }
        public int SaleCustId { get; set; }
        public int SaleCustCate { get; set; }
        public string SaleCustCode { get; set; }
        public string SaleCustName { get; set; }
        public int SaleMakerId { get; set; }
        public string SaleMakerUser { get; set; }
        public string SaleMakerName { get; set; }

        #endregion 销售信息

        #region 兑换信息

        public byte ChargeFlag { get; set; }
        public string ChargeFlagString { get; set; }
        public string ChargeDate { get; set; }
        public int ChargeCustId { get; set; }
        public int ChargeCustCate { get; set; }
        public string ChargeCustCode { get; set; }
        public string ChargeCustName { get; set; }
        public int ChargeMakerId { get; set; }
        public string ChargeMakerUser { get; set; }
        public string ChargeMakerName { get; set; }
        public decimal ChargeAmount { get; set; }
        public int Points { get; set; }

        #endregion 兑换信息
    }
}