﻿namespace Models
{
    public class ProductModel : ModelBase
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value, "Id"); }
        }

        private string _proCode;

        public string ProCode
        {
            get { return _proCode; }
            set { SetProperty(ref _proCode, value, "ProCode"); }
        }

        private string _proName;

        public string ProName
        {
            get { return _proName; }
            set { SetProperty(ref _proName, value, "ProName"); }
        }

        private string _proModel;

        public string ProModel
        {
            get { return _proModel; }
            set { SetProperty(ref _proModel, value, "ProModel"); }
        }

        private string _remark;

        public string Remark
        {
            get { return _remark; }
            set { SetProperty(ref _remark, value, "Remark"); }
        }

        private decimal _defaultPrice;

        public decimal DefaultPrice
        {
            get { return _defaultPrice; }
            set { SetProperty(ref _defaultPrice, value, "DefaultPrice"); }
        }

        private byte _proState;

        public byte ProState
        {
            get { return _proState; }
            set { SetProperty(ref _proState, value, "ProState"); }
        }
    }
}