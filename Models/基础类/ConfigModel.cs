﻿namespace Models
{
    public class ConfigModel
    {
        public string SavedUser;//保存的账号
        public string SavedPass; //保存的密码
        public bool IsSavePass; //是否保存密码
        public string ServerAddr = "127.0.0.1";//服务器地址
        public int ServerPort = 59999; //端口
    }
}