﻿using System;

namespace Models
{
    public class SessionModel
    {
        public string SessionId { get; set; }
        public int UserId { get; set; }
        public DateTime LoginTime { get; set; } = DateTime.Now;
        public UserModel UserInfo { get; set; }
        public DateTime UpdateTime { get; set; }
    }
}