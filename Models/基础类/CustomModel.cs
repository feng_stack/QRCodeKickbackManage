﻿using System;

namespace Models
{
    public class CustomCateModel
    {
        public int Id { get; set; }
        public string CateCode { get; set; }
        public string CateName { get; set; }
    }

    public class CustomModel
    {
        public int Id { get; set; }
        public int CateId { get; set; }
        public string CateName { get; set; }
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public string CustPhone { get; set; }
        public string Remark { get; set; }
        public decimal ChargeCount { get; set; }
        public decimal ChargeAmount { get; set; }
        public byte CustState { get; set; }
        public int CustPoints { get; set; }
        public int CustTotalPoints { get; set; }
        public int CustCurPoints { get { return CustTotalPoints - CustPoints; } }
    }
}