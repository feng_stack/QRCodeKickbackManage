﻿namespace Models
{
    public class UserLogModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string LogDate { get; set; }
        public string Operation { get; set; }
        public string OperateResult { get; set; }
    }
}