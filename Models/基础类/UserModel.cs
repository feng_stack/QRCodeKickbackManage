﻿namespace Models
{
    public class UserModel : ModelBase
    {
        private int _id;

        /// <summary>
        /// 用户ID
        /// </summary>
        public int Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        private string _userName;

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName
        {
            get { return _userName; }
            set { SetProperty(ref _userName, value); }
        }

        private string _password;

        /// <summary>
        /// 密码
        /// </summary>
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _realName;

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName
        {
            get { return _realName; }
            set { SetProperty(ref _realName, value); }
        }

        /// <summary>
        /// 角色 -1管理员 0编码管理员 1库管 2兑换员
        /// </summary>
        private byte _role;

        public byte Role
        {
            get { return _role; }
            set { SetProperty(ref _role, value, "Role"); }
        }

        /// <summary>
        /// 上次登录时间
        /// </summary>
        private string _lastLogin;

        public string LastLogin
        {
            get { return _lastLogin; }
            set { SetProperty(ref _lastLogin, value, "LastLogin"); }
        }

        /// <summary>
        /// 用户状态
        /// </summary>
        private byte _state;

        public byte State
        {
            get { return _state; }
            set { SetProperty(ref _state, value, "State"); }
        }

        /// <summary>
        /// 会话ID
        /// </summary>
        private string _sessionId;

        public string SessionId
        {
            get { return _sessionId; }
            set { SetProperty(ref _sessionId, value, "SessionId"); }
        }

        //Qigh,.98
    }
}