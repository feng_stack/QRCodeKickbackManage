﻿namespace Models
{
    public class RetModel
    {
        public bool Success { get; set; }
        public string ErrCode { get; set; }
        public string ErrInfo { get; set; }
        public string ReqID { get; set; }
        public object Data { get; set; }
    }
}