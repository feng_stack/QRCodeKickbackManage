﻿namespace Models
{
    public class EditParamModel
    {
        public string OpenType { get; set; }
        public int ItemId { get; set; }
        public bool SaveClose { get; set; }
    }
}