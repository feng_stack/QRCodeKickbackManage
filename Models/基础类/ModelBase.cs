﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Models
{
    public class ModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void SetProperty<T>(ref T obj, T val, [CallerMemberName] string name = "")
        {
            obj = val;
            OnPropChange(name);
        }

        public void OnPropChange(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}