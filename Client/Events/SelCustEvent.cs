﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Events;
using Models;

namespace Client.Events
{
    /// <summary>
    /// 选择客户事件
    /// </summary>
    public class SelCustEvent :PubSubEvent<CustomModel>
    {
    }
    /// <summary>
    /// 扫码事件
    /// </summary>
    public class ScanCodeEvent: PubSubEvent<string>
    {

    }
}
