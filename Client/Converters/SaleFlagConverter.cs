﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Client.Converters
{
    [ValueConversion(typeof(byte),typeof(string))]
    public class SaleFlagConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case (byte)0:
                    return "未出库";
                case (byte)1:
                    return "已出库";
                default:
                    return "未知";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case "未出库":
                    return (byte)0;
                case "已出库":
                    return (byte)1;
                default:
                    return (byte)0;
            }
        }
    }
}
