﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Client.Converters
{
    [ValueConversion(typeof(byte), typeof(string))]
    public class StatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case (byte)0:
                    return "正常";
                case (byte)1:
                    return "禁用";
                default:
                    return "状态未知";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case "正常":
                    return (byte)0;
                case "禁用":
                    return (byte)1;
                default:
                    return (byte)1;
            }
        }
    }
}
