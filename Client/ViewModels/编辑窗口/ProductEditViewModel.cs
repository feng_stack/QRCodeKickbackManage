﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Client.Common;
using System.Windows.Data;
using Prism.Mvvm;
using Prism.Commands;
using Panuon.UI.Silver;
using System.Windows;
using Client.DAL;
using Newtonsoft.Json;

namespace Client.ViewModels
{
    public class ProductEditViewModel : BindableBase
    {
        #region 绑定属性

        //标题
        private string _title;

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value, "Title"); }
        }

        //当前编辑的产品
        private ProductModel _pro;

        public ProductModel Pro
        {
            get { return _pro; }
            set { SetProperty(ref _pro, value, "Pro"); }
        }

        //显示转圈
        private Visibility _showRunning;

        public Visibility ShowRunning
        {
            get { return _showRunning; }
            set { SetProperty(ref _showRunning, value, "ShowRunning"); }
        }

        //显示转圈图片
        private bool _runCy;

        public bool RunCy
        {
            get { return _runCy; }
            set { SetProperty(ref _runCy, value, "RunCy"); }
        }

        #endregion 绑定属性

        #region 属性

        private WindowX _w;
        private string _openType = "new";
        private int _proId = 0;
        private bool _saveClose = false;

        #endregion 属性

        #region 命令

        public DelegateCommand<WindowX> LoadCommand { get; private set; }
        public DelegateCommand CloseCommand { get; private set; }
        public DelegateCommand NewCommand { get; private set; }
        public DelegateCommand SaveCommand { get; private set; }

        #endregion 命令

        #region 构造函数

        public ProductEditViewModel()
        {
            Pro = Pro ?? new ProductModel();
            LoadCommand = new DelegateCommand<WindowX>(FrmLoad);
            CloseCommand = new DelegateCommand(CloseWindow);
            NewCommand = new DelegateCommand(New);
            SaveCommand = new DelegateCommand(Save);
        }

        #endregion 构造函数

        #region 方法

        //窗口载入
        private void FrmLoad(WindowX w)
        {
            _w = w;
            Pro = Pro ?? new ProductModel();
            if (!(_w.Tag is EditParamModel em))
            {
                _openType = "new";
                Pro = new ProductModel();
                return;
            }
            _openType = em.OpenType;
            if (_openType == "new")
                New();
            else
            {
                _proId = em.ItemId;
                _saveClose = em.SaveClose;
                GetProInfo();
            }
        }

        //读产品信息
        private StateModel getProState;

        private async void GetProInfo()
        {
            if (_proId <= 0)
            {
                Comm.ShowErr("必须指定要修改的项目！", "读取产品信息");
                return;
            }
            ShowRun(true);
            void callback(RetModel rm)
            {
                if (getProState.CurStep != Steps.Send)
                    return;
                getProState.RM = rm;
                getProState.RM.Data = rm.Success ?rm.Data is null?null: JsonConvert.DeserializeObject<ProductModel>(rm.Data?.ToString()) : null;
                getProState.CurStep = rm.Success ? Steps.Succ : Steps.Fail;
            }
            getProState = new StateModel { CurStep = Steps.Send };
            ProSrv.GetProInfo(callback, _proId);
            await getProState.CheckState(10);
            if (getProState.CurStep == Steps.Succ)
            {
                if (getProState.RM.Data is ProductModel p)
                    Pro = p;
            }
            else
            {
                Comm.ShowErr(getProState.ErrStr, "读取产品信息");
            }
            ShowRun(false);
        }

        //关闭窗口
        private void CloseWindow()
        {
            _w.Close();
        }

        //新建
        private void New()
        {
            Pro = new ProductModel();
        }

        //保存
        private StateModel saveState;

        private void CheckSave()
        {
            if (Pro.ProCode.IsNullOrEmpty())
            {
                throw new Exception("产品代码必须填写！");
            }
            if (Pro.ProName.IsNullOrEmpty())
            {
                throw new Exception("产品名称必须填写！");
            }
            if (Pro.DefaultPrice < 0)
            {
                throw new Exception("默认单价不能小于零");
            }
        }

        private async void Save()
        {
            try
            {
                CheckSave();
            }
            catch (Exception ex)
            {
                Comm.ShowErr(ex.Message, "保存单据");
                return;
            }
            void callback(RetModel rm)
            {
                if (saveState.CurStep != Steps.Send) return;
                saveState.RM = rm;
                saveState.CurStep = rm.Success ? Steps.Succ : Steps.Fail;
            }

            saveState = new StateModel { CurStep = Steps.Send };
            if (_openType == "new")
                ProSrv.AddProduct(callback, Pro);
            else
                ProSrv.UpdateProduct(callback, Pro);
            await saveState.CheckState(10);
            if (saveState.CurStep == Steps.Succ)
            {
                if (_saveClose)
                {
                    CloseWindow();
                    return;
                }
                New();
            }
            else
            {
                Comm.ShowErr(saveState.ErrStr, "保存产品信息");
            }
            ShowRun(false);
        }

        private void ShowRun(bool s)
        {
            if (s)
            {
                ShowRunning = Visibility.Visible;
                RunCy = true;
            }
            else
            {
                ShowRunning = Visibility.Hidden;
                RunCy = false;
            }
        }

        #endregion 方法
    }
}