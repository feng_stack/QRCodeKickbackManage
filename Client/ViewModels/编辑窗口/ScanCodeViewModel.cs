﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using Prism.Mvvm;
using Prism.Commands;
using Prism.Events;
using System.Windows.Controls;
using Client.Events;
using System.Windows.Input;

namespace Client.ViewModels
{
    public class ScanCodeViewModel : BindableBase
    {
        #region 绑定属性

        //文本框内容
        private string _code;

        public string Code
        {
            get { return _code; }
            set { SetProperty(ref _code, value, "Code"); }
        }

        #endregion 绑定属性

        #region 属性

        private IEventAggregator _ea;

        #endregion 属性

        #region 命令

        public DelegateCommand CodeChangeCommand { get; private set; }

        #endregion 命令

        #region 构造函数

        public ScanCodeViewModel(IEventAggregator ea)
        {
            _ea = ea;
            Code = "";
            CodeChangeCommand = new DelegateCommand(CodeChange);
        }

        #endregion 构造函数

        #region 方法

        private void CodeChange()
        {
            string c = Code;
            Code = "";
            _ea.GetEvent<ScanCodeEvent>().Publish(c);
        }

        #endregion 方法
    }
}