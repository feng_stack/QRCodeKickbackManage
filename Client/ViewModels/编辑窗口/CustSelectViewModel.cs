﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Common;
using Client.DAL;
using Models;
using Prism.Mvvm;
using Prism.Commands;
using Prism.Events;
using System.Windows;
using System.Windows.Data;
using Client.Events;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace Client.ViewModels
{
    public class CustSelectViewModel : BindableBase
    {
        #region 绑定属性

        //客户列表
        private ObservableCollection<CustomModel> _custList;

        public ObservableCollection<CustomModel> CustList
        {
            get { return _custList; }
            set { SetProperty(ref _custList, value, "CustList"); }
        }

        //显示的客户列表
        private ObservableCollection<CustomModel> _showList;

        public ObservableCollection<CustomModel> ShowList
        {
            get { return _showList; }
            set { SetProperty(ref _showList, value, "ShowList"); }
        }

        //当前选择的客户
        private object _selectedCust;

        public object SelectedCust
        {
            get { return _selectedCust; }
            set { SetProperty(ref _selectedCust, value, "SelectedCust"); }
        }

        //类别列表
        private ObservableCollection<CustomCateModel> _cateList;

        public ObservableCollection<CustomCateModel> CateList
        {
            get { return _cateList; }
            set { SetProperty(ref _cateList, value, "CateList"); }
        }

        //当前选择类别
        private object _selectedCate;

        public object SelectedCate
        {
            get { return _selectedCate; }
            set
            {
                SetProperty(ref _selectedCate, value, "SelectedCate");
                if (value is null)
                    return;
                if (value is CustomCateModel cc)
                {
                    GetCustEventHandler cEv = new GetCustEventHandler(GetCustList);
                    cEv(cc.Id);
                }
            }
        }

        //查询关键字
        private string _keyword;

        public string Keyword
        {
            get { return _keyword; }
            set { SetProperty(ref _keyword, value, "Keyword"); }
        }

        #endregion 绑定属性

        #region 属性

        private IEventAggregator _ea;

        private delegate void GetCustEventHandler(int cateId);

        #endregion 属性

        #region 命令

        public DelegateCommand<Window> SelCustCommand { get; private set; }
        public DelegateCommand SearchCommand { get; private set; }

        #endregion 命令

        #region 构造函数

        public CustSelectViewModel(IEventAggregator ea)
        {
            CateList = new ObservableCollection<CustomCateModel>();
            CustList = new ObservableCollection<CustomModel>();
            ShowList = new ObservableCollection<CustomModel>();
            SelCustCommand = new DelegateCommand<Window>(SendSelectedCust);
            SearchCommand = new DelegateCommand(Search);
            _ea = ea;
            GetCateList();
        }

        #endregion 构造函数

        #region 方法

        //通知选择用户事件
        private void SendSelectedCust(Window w)
        {
            CustomModel cm = null;
            if (!(SelectedCust is null))
            {
                cm = SelectedCust as CustomModel;
            }
            _ea.GetEvent<SelCustEvent>().Publish(cm);
            w.Close();
        }

        //读客户类别
        private StateModel getListState;

        private async void GetCateList()
        {
            CateList.Clear();
            void callback(RetModel rm)
            {
                if (getListState.CurStep != Steps.Send) return;
                getListState.RM = rm;
                getListState.RM.Data = rm.Success ? rm.Data is null ? new List<CustomCateModel>() : JsonConvert.DeserializeObject<List<CustomCateModel>>(rm.Data?.ToString()) : null;
                getListState.CurStep = rm.Success ? Steps.Succ : Steps.Fail;
            }
            getListState = new StateModel { CurStep = Steps.Send };
            CustomSrv.GetCateList(callback);
            await getListState.CheckState(10);
            if (getListState.CurStep == Steps.Succ)
            {
                if (getListState.RM.Data is List<CustomCateModel> l && l?.Count > 0)
                {
                    CateList.AddRange(l);
                }
            }
        }

        //读用户列表
        private StateModel getCustState;

        private async void GetCustList(int cateId)
        {
            CustList.Clear();
            ShowList.Clear();
            if (cateId < 0)
                return;
            void callback(RetModel rm)
            {
                if (getCustState.CurStep != Steps.Send)
                    return;
                getCustState.RM = rm;
                getCustState.RM.Data = rm.Success ? rm.Data is null ? null : JsonConvert.DeserializeObject<List<CustomModel>>(rm.Data?.ToString()) : null;
                getCustState.CurStep = rm.Success ? Steps.Succ : Steps.Fail;
            }
            getCustState = new StateModel { CurStep = Steps.Send };
            CustomSrv.GetCustList(callback, cateId, 0);
            await getCustState.CheckState(10);
            if (getCustState.CurStep == Steps.Succ)
            {
                if (getCustState.RM.Data is List<CustomModel> l && l?.Count > 0)
                {
                    CustList.AddRange(l);
                    ShowList.AddRange(l);
                }
            }
            SelectedCust = null;
        }

        //查找
        private void Search()
        {
            if (CustList.Count < 1)
                return;
            ShowList.Clear();
            if (string.IsNullOrEmpty(Keyword))
            {
                ShowList.AddRange(CustList);
                return;
            }
            var l = CustList.Where(p => p.CustPhone.Contains(Keyword))?.ToList();
            if (l != null)
            {
                ShowList.AddRange(l);
            }
        }

        #endregion 方法
    }
}