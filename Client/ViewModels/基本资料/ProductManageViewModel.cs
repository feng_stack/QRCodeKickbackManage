﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;

using Client.Common;
using Client.DAL;

using Models;

using Newtonsoft.Json;

using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;

namespace Client.ViewModels
{
    public class ProductManageViewModel : BindableBase, IRegionMemberLifetime
    {
        #region 绑定属性

        //产品编号
        private string _searchProCode;

        public string SearchProCode
        {
            get { return _searchProCode; }
            set { SetProperty(ref _searchProCode, value, "SearchProCode"); }
        }

        //产品名称
        private string _searchProName;

        public string SearchProName
        {
            get { return _searchProName; }
            set { SetProperty(ref _searchProName, value, "SearchProName"); }
        }

        //是否包含作废
        private bool _containCancel;

        public bool ContainCancel
        {
            get { return _containCancel; }
            set { SetProperty(ref _containCancel, value, "ContainCancel"); }
        }

        //查询结果列表
        private ObservableCollection<ProductModel> _proList;

        public ObservableCollection<ProductModel> ProList
        {
            get { return _proList; }
            set { SetProperty(ref _proList, value, "ProList"); }
        }

        //当前选择的产品
        private object _selectedPro;

        public object SelectedPro
        {
            get { return _selectedPro; }
            set { SetProperty(ref _selectedPro, value, "SelectedPro"); }
        }

        //载入界面状态
        private Visibility _showRunning;

        public Visibility ShowRunning
        {
            get { return _showRunning; }
            set { SetProperty(ref _showRunning, value, "ShowRunning"); }
        }

        //查询按钮工作状态
        private bool _btnQueryWorking;

        public bool BtnQueryWorking
        {
            get { return _btnQueryWorking; }
            set { SetProperty(ref _btnQueryWorking, value, "BtnQueryWorking"); }
        }

        private bool _runCy;

        public bool RunCy
        {
            get { return _runCy; }
            set { SetProperty(ref _runCy, value, "RunCy"); }
        }

        #endregion 绑定属性

        #region 属性

        #endregion 属性

        #region 命令

        public DelegateCommand QueryCommand { get; private set; }
        public DelegateCommand AddCommand { get; private set; }
        public DelegateCommand EditCommand { get; private set; }
        public DelegateCommand EnableCommand { get; private set; }
        public DelegateCommand DisabledCommand { get; private set; }
        public DelegateCommand RefreshCommand { get; private set; }

        #endregion 命令

        #region 构造函数

        public ProductManageViewModel()
        {
            //初始值
            ProList = new ObservableCollection<ProductModel>();
            SearchProCode = "";
            SearchProName = "";
            ShowRun(false);
            //命令
            QueryCommand = new DelegateCommand(GetProList);
            AddCommand = new DelegateCommand(Add);
            EditCommand = new DelegateCommand(Edit);
            DisabledCommand = new DelegateCommand(DePro);
            RefreshCommand = new DelegateCommand(GetProList);
            EnableCommand = new DelegateCommand(EnPro);
        }

        #endregion 构造函数

        #region 方法

        #region 查询列表

        private StateModel getProListState;

        private async void GetProList()
        {
            ShowRun(true);
            string err = "";
            ProList.Clear();
            List<ProductModel> l = null;
            try
            {
                getProListState = new StateModel { CurStep = Steps.Send };
                ProSrv.GetProList(GetProListCallback, SearchProCode, SearchProName, ContainCancel ? (byte)255 : (byte)0);
                await getProListState.CheckState(10);
                l = GenProList();
            }
            catch (Exception ex)
            {
                Comm.ShowErr(ex.Message, "查询产品");
                ShowRun(false);
                return;
            }
            if (l is null)
            {
                Comm.ShowErr(err, "查询产品");
            }
            else
            {
                if (l.Count > 0)
                    ProList.AddRange(l);
            }
            ShowRun(false);
        }

        private List<ProductModel> GenProList()
        {
            try
            {
                if (getProListState.RM is null)
                {
                    getProListState.RM = new RetModel { ErrCode = "0000", ErrInfo = "无法解析返回数据" };
                    return null;
                }
                List<ProductModel> l = JsonConvert.DeserializeObject<List<ProductModel>>(getProListState.RM.Data.ToString());
                return l;
            }
            catch (Exception ex)
            {
                getProListState.RM = new RetModel { ErrCode = "0000", ErrInfo = ex.Message };
                return null;
            }
        }

        private void GetProListCallback(RetModel rm)
        {
            if (getProListState.CurStep != Steps.Send)
                return;
            getProListState.RM = rm;
            getProListState.CurStep = rm.Success ? Steps.Succ : Steps.Fail;
        }

        #endregion 查询列表

        #region 增加按钮

        private void Add()
        {
            Views.ProductEditView w = new Views.ProductEditView
            {
                Tag = new EditParamModel { ItemId = 0, OpenType = "new", SaveClose = false },
                Owner = Comm.Shell,
                Title = "增加产品"
            };
            w.ShowDialog();
            GetProList();
        }

        #endregion 增加按钮

        #region 修改

        private void Edit()
        {
            if (SelectedPro is null)
            {
                Comm.ShowErr("必须选择要修改的产品！", "产品修改", Panuon.UI.Silver.MessageBoxIcon.Error);
                return;
            }
            if (!(SelectedPro is ProductModel p))
            {
                Comm.ShowErr("选择的项目不正确！", "产品修改");
                return;
            }
            Views.ProductEditView w = new Views.ProductEditView
            {
                Title = "产品信息修改",
                Owner = Comm.Shell,
                Tag = new EditParamModel { ItemId = p.Id, OpenType = "edit", SaveClose = true }
            };
            w.ShowDialog();
            GetProList();
        }

        #endregion 修改

        #region 禁用

        private StateModel enableProState;

        private async void DePro()
        {
            if (SelectedPro is null)
            {
                Comm.ShowErr("必须选择要禁用的产品", "产品禁用");
                return;
            }
            if (!(SelectedPro is ProductModel p))
            {
                Comm.ShowErr("选择的产品不正确！", "产品禁用");
                return;
            }
            ShowRun(true);
            try
            {
                enableProState = new StateModel { CurStep = Steps.Send };
                ProSrv.DisablePro(DisableProCallback, p.Id);
                await enableProState.CheckState(10);
            }
            catch (Exception ex)
            {
                Comm.ShowErr(ex.Message, "产品禁用");
                ShowRun(false);
                return;
            }
            if (enableProState.CurStep == Steps.Succ)
            {
                GetProList();
            }
            else
            {
                Comm.ShowErr(enableProState.ErrStr, "产品禁用");
            }
            ShowRun(false);
        }

        private void DisableProCallback(RetModel rm)
        {
            if (enableProState.CurStep != Steps.Send)
                return;
            enableProState.RM = rm;
            enableProState.CurStep = rm.Success ? Steps.Succ : Steps.Fail;
        }

        #endregion 禁用

        #region 启用产品

        private async void EnPro()
        {
            if (SelectedPro is null)
            {
                Comm.ShowErr("必须选择要启用的产品", "产品启用");
                return;
            }
            if (!(SelectedPro is ProductModel p))
            {
                Comm.ShowErr("选择的产品不正确！", "产品启用");
                return;
            }
            ShowRun(true);
            try
            {
                enableProState = new StateModel { CurStep = Steps.Send };
                ProSrv.EnablePro(EnableProCallback, p.Id);
                await enableProState.CheckState(10);
            }
            catch (Exception ex)
            {
                Comm.ShowErr(ex.Message, "产品启用");
                ShowRun(false);
                return;
            }

            if (enableProState.CurStep == Steps.Succ)
            {
                GetProList();
            }
            else
            {
                Comm.ShowErr(enableProState.ErrStr, "产品启用");
            }
            ShowRun(false);
        }

        private void EnableProCallback(RetModel rm)
        {
            if (enableProState.CurStep != Steps.Send)
                return;
            enableProState.RM = rm;
            enableProState.CurStep = rm.Success ? Steps.Succ : Steps.Fail;
        }

        #endregion 启用产品

        #region 显示运行中状态

        private void ShowRun(bool s)
        {
            if (s)
            {
                ShowRunning = Visibility.Visible;
                RunCy = true;
            }
            else
            {
                ShowRunning = Visibility.Hidden;
                RunCy = false;
            }
        }

        #endregion 显示运行中状态

        #endregion 方法

        public bool KeepAlive => false;
    }
}