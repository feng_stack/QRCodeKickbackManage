﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Prism.Regions;
using Prism.Commands;
using Client.Common;
using Client.DAL;
using System.Windows;
using Prism.Events;
using Client.Events;

namespace Client.ViewModels
{
    public class PointBillViewModel : BindableBase, IRegionMemberLifetime
    {
        #region 绑定属性

        public bool KeepAlive => false;
        private CustomModel _curCust;

        //当前客户信息
        public CustomModel CurCust
        {
            get { return _curCust; }
            set { SetProperty(ref _curCust, value, nameof(CurCust)); }
        }

        //使用积分
        private int _usePoint;

        public int UsePoint
        {
            get { return _usePoint; }
            set { SetProperty(ref _usePoint, value, nameof(UsePoint)); }
        }

        //兑换备注
        private string _remark;

        public string Remark
        {
            get { return _remark; }
            set { SetProperty(ref _remark, value, nameof(Remark)); }
        }

        //运行中边框
        private Visibility _running;

        public Visibility Running
        {
            get { return _running; }
            set { SetProperty(ref _running, value, nameof(Running)); }
        }

        //运行中转圈图标
        private bool _runCy;

        public bool RunCy
        {
            get { return _runCy; }
            set { SetProperty(ref _runCy, value, nameof(RunCy)); }
        }

        #endregion 绑定属性

        #region 命令

        public DelegateCommand NewCommand { get; private set; }
        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand SelCustCommand { get; private set; }

        #endregion 命令

        #region 构造函数

        private readonly IEventAggregator _ea;

        public PointBillViewModel(IEventAggregator ea)
        {
            _ea = ea;
            _curCust = new CustomModel();
            _remark = "";
            _usePoint = 0;
            NewCommand = new DelegateCommand(NewBill);
            SaveCommand = new DelegateCommand(SaveBill);
            SelCustCommand = new DelegateCommand(ShowCustSelectWindow);
            _ea?.GetEvent<SelCustEvent>().Subscribe(ProcessCustomSelect);
            Running = Visibility.Hidden;
            RunCy = false;
        }

        #endregion 构造函数

        #region 方法

        #region 显示转圈图标

        private void ShowRun()
        {
            Running = Visibility.Visible;
            RunCy = true;
        }

        private void HideRun()
        {
            Running = Visibility.Hidden;
            RunCy = false;
        }

        #endregion 显示转圈图标

        #region 选择客户

        private void ShowCustSelectWindow()
        {
            Window w = new Views.CustSelectView()
            {
                Owner = Comm.Shell,
                Title = "选择客户"
            };
            w.ShowDialog();
        }

        private void ProcessCustomSelect(CustomModel cm)
        {
            if (cm != null)
                CurCust = cm;
        }

        #endregion 选择客户

        #region 新单据

        private void NewBill()
        {
            CurCust = new CustomModel();
            UsePoint = 0;
            Remark = "";
        }

        #endregion 新单据

        #region 保存单据

        private void SaveBill()
        {
            if (CurCust is null)
                throw new Exception(Properties.Resources.NeedSelectCustom);
            if (CurCust.Id <= 0)
                throw new Exception(Properties.Resources.NeedCustom);
            if (UsePoint <= 0)
                throw new Exception(Properties.Resources.PBill_NeedUsePoint);
            ShowRun();
            PointBillModel pm = new PointBillModel
            {
                CustId = CurCust.Id,
                MakerId = Comm.UserInfo.Id,
                Remark = Remark,
                UsePoint = UsePoint
            };
            BillSrv.SavePointBill(SaveBillCallback, pm);
        }

        private void SaveBillCallback(RetModel rm)
        {
            try
            {
                if (rm.Success)
                {
                    NewBill();
                    HideRun();
                }
                else
                {
                    throw new Exception(rm.ErrInfo);
                }
            }
            catch (Exception ex)
            {
                Application.Current.Dispatcher.Invoke(new Action(() => Comm.ShowErr(ex.Message, "保存单据")));
                HideRun();
            }
        }

        #endregion 保存单据

        #endregion 方法
    }
}