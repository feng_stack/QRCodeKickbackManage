﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;
using Prism.Commands;
using Client.Common;
using Models;
using System.Windows;

namespace Client.ViewModels
{
    public class ServerSetViewModel : BindableBase
    {
        #region 绑定属性

        private string _serverAddr;

        public string ServerAddr
        {
            get { return _serverAddr; }
            set { SetProperty(ref _serverAddr, value, "ServerAddr"); }
        }

        private int _serverPort;

        public int ServerPort
        {
            get { return _serverPort; }
            set { SetProperty(ref _serverPort, value, "ServerPort"); }
        }

        #endregion 绑定属性

        #region 命令

        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand<Window> LoadCommand { get; private set; }

        #endregion 命令

        #region 属性

        private Window _w;

        #endregion 属性

        #region 构造函数

        public ServerSetViewModel()
        {
            Comm.Running = false;
            ServerAddr = Comm.CFG.ServerAddr;
            ServerPort = Comm.CFG.ServerPort;
            SaveCommand = new DelegateCommand(Save);
            LoadCommand = new DelegateCommand<Window>(WLoad);
        }

        #endregion 构造函数

        #region 方法

        private void WLoad(Window w)
        {
            _w = w;
        }

        private void Save()
        {
            Comm.CFG.ServerAddr = ServerAddr;
            Comm.CFG.ServerPort = ServerPort;
            Comm.SaveConfig();
            Comm.Running = true;
            _w.Close();
        }

        #endregion 方法
    }
}