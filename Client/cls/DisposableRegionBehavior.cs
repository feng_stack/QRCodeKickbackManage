﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Prism.Regions;

namespace Client.cls
{
    public class DisposableRegionBehavior : RegionBehavior
    {
        public const string BehaviorKey = "DisposableRegion";

        protected override void OnAttach()
        {
            this.Region.Views.CollectionChanged += OnActiveViewsChanged;
        }

        private void OnActiveViewsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                DisposeViewsOrViewModels(e.OldItems);
            }
        }

        private static void DisposeViewsOrViewModels(IList views)
        {
            foreach (var view in views)
            {
                if (view is FrameworkElement frameworkElement)
                {
                    if (frameworkElement.DataContext is IDisposable disposableDataContext)
                        disposableDataContext.Dispose();

                    if (view is IDisposable)
                    {
                        var disposableView = view as IDisposable;
                        disposableView.Dispose();
                    }
                }
            }
        }
    }
}
