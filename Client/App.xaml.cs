﻿using System.Windows;
using Prism.Ioc;
using Prism.Unity;
using Client.Common;
using System.Windows.Navigation;
using Prism.Regions;

namespace Client
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : PrismApplication
    {
        #region 公共变量

        #endregion 公共变量

        protected override Window CreateShell()
        {
            return Container.Resolve<Views.MainWindow>();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            Comm.Running = false;
            base.OnExit(e);
        }

        protected override void InitializeShell(Window shell)
        {
            Comm.ReadConfig();
            Views.LoginView l = new Views.LoginView();
            ViewModels.LoginViewModel lm = new ViewModels.LoginViewModel(l.Close);
            lm.Win = l;
            l.DataContext = lm;
            l.ShowDialog();
            Comm.Shell = shell;
            if (Comm.UserInfo is null)
                shell?.Close();
            else
                base.InitializeShell(shell);
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<Views.CodeGenerateView>();
            containerRegistry.RegisterForNavigation<Views.CodeManageView>();
            containerRegistry.RegisterForNavigation<Views.ProductManageView>();
            containerRegistry.RegisterForNavigation<Views.CustomManageView>();
            containerRegistry.RegisterForNavigation<Views.SaleBillView>();
            containerRegistry.RegisterForNavigation<Views.ChargeBillView>();
            containerRegistry.RegisterForNavigation<Views.UserManageView>();
            containerRegistry.RegisterForNavigation<Views.ChargeDetailRptView>();
            containerRegistry.RegisterForNavigation<Views.SaleDetailRptView>();
            containerRegistry.RegisterForNavigation<Views.ProSumRptView>();
            containerRegistry.RegisterForNavigation<Views.SaleSumRptView>();
            containerRegistry.RegisterForNavigation<Views.ChargeSumRptView>();
            containerRegistry.RegisterForNavigation<Views.PointBillView>();
        }
    }
}