﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Server.DAL;

namespace Server.Checkers
{
    public class ProChecker
    {
        public ProductModel ProInfo;
        public int ProId;
        public string ProCode;

        #region 检查数据合法性

        public void CheckInfo()
        {
            CheckInfoNull();
            CheckInfoCode();
            CheckInfoName();
            CheckInfoExist();
        }

        private void CheckInfoNull()
        {
            if (ProInfo is null)
                throw new Exception("数据不能为空");
        }

        private void CheckInfoCode()
        {
            if (string.IsNullOrEmpty(ProInfo.ProCode))
                throw new Exception("编号不能为空");
        }

        private void CheckInfoName()
        {
            if (string.IsNullOrEmpty(ProInfo.ProName))
                throw new Exception("产品名称不能为空");
        }

        private void CheckInfoExist()
        {
            ProductModel pro = ProSrv.GetProInfo(ProInfo.ProCode);
            if (pro != null && pro?.Id != ProInfo.Id)
                throw new Exception("编码重复");
        }

        #endregion 检查数据合法性

        #region 检查查询条件

        public void CheckID()
        {
            if (ProId <= 0)
                throw new Exception("必须指定产品");
        }

        public void CheckIdAndCode()
        {
            if (ProId <= 0 && string.IsNullOrEmpty(ProCode))
                throw new Exception("必须指定产品");
        }

        #endregion 检查查询条件
    }
}