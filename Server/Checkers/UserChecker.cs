﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Server.DAL;

namespace Server.Checkers
{
    public class UserChecker
    {
        public UserModel UserInfo;
        public int UserId;
        public string UserName;
        public string OPass;
        public string NPass;

        #region 检查数据合法性

        public void CheckUserInfo()
        {
            CheckUserNull();
            CheckUserName();
            CheckUserPass();
            CheckExist();
        }

        public void CheckUserNull()
        {
            if (UserInfo is null)
                throw new Exception("数据不能为空");
        }

        private void CheckUserName()
        {
            if (string.IsNullOrEmpty(UserInfo.UserName))
                throw new Exception("用户名不能为空");
        }

        private void CheckUserPass()
        {
            if (string.IsNullOrEmpty(UserInfo.Password))
                throw new Exception("密码不能为空");
        }

        private void CheckExist()
        {
            UserModel um = UserSrv.GetUserInfo(UserInfo.UserName);
            if (um != null && um?.Id != UserInfo.Id)
                throw new Exception("用户名重复");
        }

        #endregion 检查数据合法性

        #region 检查ID用户名

        public void CheckID()
        {
            if (UserId <= 0)
                throw new Exception("必须指定用户");
        }

        public void CheckIdAndName()
        {
            if (UserId <= 0 && string.IsNullOrEmpty(UserName))
                throw new Exception("必须指定用户");
        }

        public void CheckPassword()
        {
            UserModel um = UserSrv.GetUserInfo(UserId);
            if (um.Password != OPass)
                throw new Exception("密码不正确！");
        }

        public void CheckEditPass()
        {
            if (string.IsNullOrEmpty(OPass) || string.IsNullOrEmpty(NPass))
                throw new Exception("新密码、原密码不能为空");
            CheckPassword();
        }

        public void CheckUserExist()
        {
            UserModel um = UserSrv.GetUserInfo(UserName);
            if (um is null)
                throw new Exception("用户不存在");
        }

        #endregion 检查ID用户名
    }
}