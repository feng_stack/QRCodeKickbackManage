﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Server.Checkers
{
    public class CustCateChecker
    {
        public CustomCateModel Cate;
        public int CateId;
        public string CateCode;

        #region 检查数据填写完整

        public void CheckCateInfo()
        {
            CheckCateNull();
            CheckCode();
            CheckName();
            CheckExist();
        }

        private void CheckCateNull()
        {
            if (Cate is null)
                throw new Exception("数据不能为空");
        }

        private void CheckCode()
        {
            if (string.IsNullOrEmpty(Cate.CateCode))
                throw new Exception("类别编码不能为空");
        }

        private void CheckName()
        {
            if (string.IsNullOrEmpty(Cate.CateName))
                throw new Exception("类别名称不能为空");
        }

        private void CheckExist()
        {
            CustomCateModel cm = DAL.CustomSrv.GetCateInfo(Cate.CateCode);
            if (cm != null && cm?.Id != Cate.Id)
                throw new Exception("编码重复");
        }

        #endregion 检查数据填写完整

        #region 数据合法性

        public void CheckID()
        {
            if (CateId <= 0)
                throw new Exception("必须指定类别");
        }

        public void CheckDel()
        {
            CheckID();
            int childCount = DAL.CustomSrv.GetCateChildCount(CateId);
            if (childCount > 0)
                throw new Exception("不能删除有明细的类别");
        }

        public void CheckIdAndCode()
        {
            if (CateId <= 0 && string.IsNullOrEmpty(CateCode))
                throw new Exception("必须指定类别");
        }

        #endregion 数据合法性
    }
}