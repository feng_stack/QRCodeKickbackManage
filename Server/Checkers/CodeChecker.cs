﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Server.Checkers
{
    public class CodeChecker
    {
        public CodeModel CodeInfo;
        public int CodeId;
        public string CodeFull;
        public List<CodeModel> CodeList;

        #region 检查读信息条件

        public void CheckCodeAndId()
        {
            if (CodeId <= 0 && string.IsNullOrEmpty(CodeFull))
                throw new Exception("查询条件不正确！");
        }

        #endregion 检查读信息条件

        #region 检查ID

        public void CheckId()
        {
            if (CodeId <= 0)
                throw new Exception("必须指定编码！");
        }

        #endregion 检查ID

        #region 检查编码

        public void CheckCode()
        {
            CheckNull();
            CheckPro();
            CheckNum();
            CheckUser();
        }

        private void CheckNull()
        {
            if (CodeInfo is null)
                throw new Exception("编码不能为空");
        }

        private void CheckPro()
        {
            if (CodeInfo.ProId <= 0)
                throw new Exception("必须指定产品");
        }

        private void CheckNum()
        {
            if (CodeInfo.CodeMain?.Length != 8)
                throw new Exception("随机码长度不正确！");
        }

        private void CheckUser()
        {
            if (CodeInfo.OperatorId <= 0)
                throw new Exception("操作人不能为空！");
        }

        #endregion 检查编码

        #region 检查编码列表

        public void CheckCodeList()
        {
            CheckListNull();
            foreach (var item in CodeList)
            {
                CodeInfo = item;
                CheckCode();
            }
        }

        private void CheckListNull()
        {
            if (CodeList is null)
                throw new Exception("数据不能为空！");
        }

        #endregion 检查编码列表
    }
}