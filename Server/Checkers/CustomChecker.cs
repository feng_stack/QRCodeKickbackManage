﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Server.DAL;

namespace Server.Checkers
{
    public class CustomChecker
    {
        public CustomModel CustInfo;
        public int CustId;
        public string CustCode;

        #region 检查数据合法性

        public void CheckData()
        {
            CheckInfoNull();
            CheckInfoCode();
            CheckInfoName();
            CheckInfoExist();
        }

        private void CheckInfoNull()
        {
            if (CustInfo is null)
                throw new Exception("数据不能为空");
        }

        private void CheckInfoCode()
        {
            if (string.IsNullOrEmpty(CustInfo.CustCode))
                throw new Exception("编号不能为空");
        }

        private void CheckInfoName()
        {
            if (string.IsNullOrEmpty(CustInfo.CustName))
                throw new Exception("名称不能为空");
        }

        private void CheckInfoExist()
        {
            CustomModel cm = CustomSrv.GetCustInfo(CustInfo.CustCode);
            if (cm != null && cm?.Id != CustInfo.Id)
                throw new Exception("编号已经存在");
        }

        #endregion 检查数据合法性

        #region 检查ID和编号

        public void CheckID()
        {
            if (CustId <= 0)
                throw new Exception("必须指定客户");
        }

        public void CheckIdAndCode()
        {
            if (CustId <= 0 && string.IsNullOrEmpty(CustCode))
                throw new Exception("条件不正确");
        }

        #endregion 检查ID和编号
    }
}