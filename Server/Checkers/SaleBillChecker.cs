﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Server.Checkers
{
    public class SaleBillChecker
    {
        public CustomModel Cust;
        public List<BillEntryModel> Entry;

        #region 检查客户信息

        public void CheckCustom()
        {
            CheckCustNull();
            CheckCustId();
        }

        private void CheckCustNull()
        {
            if (Cust is null)
                throw new Exception("客户不能为空");
        }

        private void CheckCustId()
        {
            if (Cust.Id <= 0)
                throw new Exception("客户必须选择");
        }

        #endregion 检查客户信息

        #region 检查单据信息

        public void CheckBillEntry()
        {
            CheckEntryNull();
            foreach (var item in Entry)
            {
                CheckCode(item);
                BillEntryModel bm = DAL.BillSrv.GetCodeInfo(item.CodeFull);
                CheckSearch(bm);
                CheckSaleState(bm);
            }
        }

        private void CheckEntryNull()
        {
            if (Entry is null)
                throw new Exception("数据不能为空！");
        }

        private void CheckCode(BillEntryModel bm)
        {
            if (string.IsNullOrEmpty(bm.CodeFull))
                throw new Exception("编号不能为空");
        }

        private void CheckSearch(BillEntryModel bm)
        {
            if (bm is null)
                throw new Exception("未找到指定编号 ");
        }

        private void CheckSaleState(BillEntryModel bm)
        {
            if (bm.SaleFlag != 0)
                throw new Exception($"编号：{bm.CodeFull}已经出库，不能重复出库");
        }

        #endregion 检查单据信息
    }
}