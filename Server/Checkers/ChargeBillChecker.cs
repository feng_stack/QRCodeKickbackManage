﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Server.Checkers
{
    public class ChargeBillChecker
    {
        public List<BillEntryModel> Entry;
        public CustomModel Cust;

        #region 检查单据明细

        public void CheckEntries()
        {
            CheckEntryNull();
            foreach (var item in Entry)
            {
                CheckEntryCode(item);
                BillEntryModel bm = DAL.BillSrv.GetCodeInfo(item.CodeFull);
                CheckSaleState(bm);
                CheckChargeState(bm);
            }
        }

        private void CheckEntryNull()
        {
            if (null == Entry)
                throw new Exception("数据不能为空！");
        }

        private void CheckEntryCode(BillEntryModel bm)
        {
            if (string.IsNullOrEmpty(bm.CodeFull))
                throw new Exception("编码不能为空！");
        }

        private void CheckSaleState(BillEntryModel bm)
        {
            if (bm.SaleFlag != 1)
                throw new Exception($"编号：{bm.CodeFull}还未出库，不能核销");
        }

        private void CheckChargeState(BillEntryModel bm)
        {
            if (bm.ChargeFlag != 0)
                throw new Exception($"编号：{bm.CodeFull}已经兑换过，不能重复兑换！");
        }

        #endregion 检查单据明细

        #region 检查客户信息

        public void CheckCustom()
        {
            CheckCustNull();
            CheckCustId();
        }

        private void CheckCustNull()
        {
            if (Cust is null)
                throw new Exception("客户信息不能为空！");
        }

        private void CheckCustId()
        {
            if (Cust.Id <= 0)
                throw new Exception("必须选择客户！");
        }

        #endregion 检查客户信息
    }
}