﻿using System;
using System.Collections.Generic;

using Models;

using Server.Checkers;
using Server.Common;
using Server.DAL;
using Taxhui.Utils.CryptUtils;

namespace Server.BLL
{
    public static class UserBLL
    {
        #region 增加用户

        public static RetModel AddUser(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                UserModel userInfo = Comm.ResolveClass<UserModel>(dc, "userInfo");
                UserChecker checker = new UserChecker { UserInfo = userInfo };
                checker.CheckUserInfo();
                UserSrv.AddUser(userInfo);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "501";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 增加用户

        #region 修改用户

        public static RetModel EditUser(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                UserModel um = Comm.ResolveClass<UserModel>(dc, "userInfo");
                UserChecker checker = new UserChecker { UserInfo = um };
                checker.CheckUserInfo();
                checker.UserId = um.Id;
                checker.CheckID();
                UserSrv.EditUser(um);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "504";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 修改用户

        #region 禁用用户

        public static RetModel DisableUser(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int userId = Comm.ResolveParam<int>(dc, "userId", 0);
                UserChecker checker = new UserChecker { UserId = userId };
                checker.CheckID();
                if (userId > 1) UserSrv.DisableUser(userId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "502";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 禁用用户

        #region 启用用户

        public static RetModel EnableUser(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int userId = Comm.ResolveParam<int>(dc, "userId", 0);
                UserChecker checker = new UserChecker { UserId = userId };
                checker.CheckID();
                if (userId > 1) UserSrv.EnableUser(userId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "503";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 启用用户

        #region 修改密码

        public static RetModel EditPass(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                string oldPass = Comm.ResolveParam<string>(dc, "oldPass", "");
                string newPass = Comm.ResolveParam<string>(dc, "newPass", "");
                int userId = (dc["sess"] as SessionModel).UserId;
                UserChecker checker = new UserChecker { UserId = userId, OPass = oldPass, NPass = newPass };
                checker.CheckID();
                checker.CheckEditPass();
                UserSrv.EditPass(newPass, userId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "505";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 修改密码

        #region 读用户信息

        public static RetModel GetUserInfo(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int userId = Comm.ResolveParam<int>(dc, "userId", 0);
                string userName = Comm.ResolveParam<string>(dc, "userName", "");
                UserChecker checker = new UserChecker { UserId = userId, UserName = userName };
                checker.CheckIdAndName();
                rm.Data = userId > 0 ? UserSrv.GetUserInfo(userId) : UserSrv.GetUserInfo(userName);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "506";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读用户信息

        #region 读用户列表

        public static RetModel GetUserList(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                byte state = Comm.ResolveParam<byte>(dc, "state", 0);
                rm.Data = UserSrv.GetUserList(state);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "507";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读用户列表

        #region 登录

        public static RetModel Login(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                UserModel userInfo = Comm.ResolveClass<UserModel>(dc, "userInfo");
                UserChecker checker = new UserChecker { UserInfo = userInfo };
                CheckDefaultUser();

                checker.CheckUserNull();

                userInfo = UserSrv.GetUserInfo(userInfo.UserName);
                checker.CheckUserNull();

                checker.UserName = userInfo.UserName;
                checker.OPass = userInfo.Password;
                checker.UserId = userInfo.Id;
                checker.CheckPassword();

                SessionModel sess = new SessionModel
                {
                    SessionId = Taxhui.Utils.CryptUtils.MD5Crypt.MD32Crypt(userInfo.UserName + DateTime.UtcNow.Ticks.ToString()),
                    UpdateTime = DateTime.Now,
                    LoginTime = DateTime.Now,
                    UserId = userInfo.Id,
                    UserInfo = userInfo
                };
                Comm.AddOnline(sess);
                UserSrv.UpdateLoginTime(userInfo.Id);
                userInfo.SessionId = sess.SessionId;
                LogSrv.Write(userInfo.Id, "登录", "登录成功");
                rm.Data = userInfo;
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "508";
                rm.ErrInfo = ex.Message;
            }

            return rm;
        }

        private static void CheckDefaultUser()
        {
            var lst = UserSrv.GetUserList(255);
            if (null == lst || lst?.Count < 1)
            {
                UserModel umAdmin = new UserModel
                {
                    UserName = "admin",
                    Password = MD5Crypt.MD32Crypt("123"),
                    RealName = "店长"
                };
                UserSrv.AddUser(umAdmin);
            }
        }

        #endregion 登录

        #region 心跳

        public static RetModel HeartBeat(Dictionary<string, object> dc = null)
        {
            return new RetModel { Success = true };
        }

        #endregion 心跳
    }
}