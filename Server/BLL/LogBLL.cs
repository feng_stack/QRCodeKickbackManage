﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Server.Checkers;
using Server.DAL;
using Models;
using Server.Common;

namespace Server.BLL
{
    public static class LogBLL
    {
        #region 清空LOG

        public static RetModel ClearLog(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int uId = Comm.ResolveParam<int>(dc, "uId", 0);
                string sDate = Comm.ResolveParam<string>(dc, "sDate", null);
                string eDate = Comm.ResolveParam<string>(dc, "eDate", null);
                string operate = Comm.ResolveParam<string>(dc, "operate", null);
                sDate = string.IsNullOrEmpty(sDate) ? "" : Convert.ToDateTime(sDate).ToString("yyyy-MM-dd 00:00:00");
                eDate = string.IsNullOrEmpty(eDate) ? "" : Convert.ToDateTime(eDate).ToString("yyyy-MM-dd 23:59:59");
                LogSrv.ClearLog(uId, sDate, eDate, operate);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "201";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 清空LOG

        #region 读取日志信息

        public static RetModel GetLog(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int logId = Comm.ResolveParam<int>(dc, "logId", 0);
                rm.Data = LogSrv.GetLog(logId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "201";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读取日志信息

        #region 读取日志列表

        public static RetModel GetLogList(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int uId = Comm.ResolveParam<int>(dc, "uId", 0);
                string sDate = Comm.ResolveParam<string>(dc, "sDate", null);
                string eDate = Comm.ResolveParam<string>(dc, "eDate", null);
                string operate = Comm.ResolveParam<string>(dc, "operate", null);
                sDate = string.IsNullOrEmpty(sDate) ? "" : Convert.ToDateTime(sDate).ToString("yyyy-MM-dd 00:00:00");
                eDate = string.IsNullOrEmpty(eDate) ? "" : Convert.ToDateTime(eDate).ToString("yyyy-MM-dd 23:59:59");
                rm.Data = LogSrv.GetLogList(uId, sDate, eDate, operate);
            }
            catch (Exception ex)
            {
                rm.ErrCode = "203";
                rm.ErrInfo = ex.Message;
                rm.Success = false;
            }
            return rm;
        }

        #endregion 读取日志列表

        #region 写日志

        public static RetModel Write(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int uId = Comm.ResolveParam<int>(dc, "uId", 0);
                string operate = Comm.ResolveParam<string>(dc, "operate", "");
                string result = Comm.ResolveParam<string>(dc, "result", "");
                if (uId <= 0 || string.IsNullOrEmpty(operate))
                {
                    rm.Success = false;
                    return rm;
                }
                LogSrv.Write(uId, operate, result);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "204";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 写日志
    }
}