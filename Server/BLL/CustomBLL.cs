﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Server.DAL;
using Server.Common;
using Server.Checkers;

namespace Server.BLL
{
    public static class CustomBLL
    {
        #region 增加类别

        public static RetModel AddCate(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                CustomCateModel cateInfo = Comm.ResolveClass<CustomCateModel>(dc, "cateInfo");
                CustCateChecker checker = new CustCateChecker { Cate = cateInfo };
                checker.CheckCateInfo();
                CustomSrv.AddCate(cateInfo);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "301";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 增加类别

        #region 修改类别

        public static RetModel EditCate(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                CustomCateModel cateInfo = Comm.ResolveClass<CustomCateModel>(dc, "cateInfo");
                CustCateChecker checker = new CustCateChecker { Cate = cateInfo };
                checker.CheckCateInfo();
                CustomSrv.EditCate(cateInfo);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "302";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 修改类别

        #region 删除类别

        public static RetModel DelCate(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int cateId = Comm.ResolveParam<int>(dc, "cateId", 0);
                CustCateChecker checker = new CustCateChecker { CateId = cateId };
                checker.CheckDel();
                CustomSrv.DelCate(cateId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "303";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 删除类别

        #region 读取类别信息

        public static RetModel GetCateInfo(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int cateId = Comm.ResolveParam<int>(dc, "cateId", 0);
                string cateCode = Comm.ResolveParam<string>(dc, "cateCode", "");
                CustCateChecker checker = new CustCateChecker { CateId = cateId };
                checker.CheckIdAndCode();
                rm.Data = cateId > 0 ? CustomSrv.GetCateInfo(cateId) : CustomSrv.GetCateInfo(cateCode);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "305";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读取类别信息

        #region 读取类别列表

        public static RetModel GetCateList(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                rm.Data = CustomSrv.GetCateList();
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "306";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读取类别列表

        #region 增加客户

        public static RetModel AddCust(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                CustomModel cm = Comm.ResolveClass<CustomModel>(dc, "custInfo");
                CustomChecker checker = new CustomChecker { CustInfo = cm };
                checker.CheckData();
                CustomSrv.AddCust(cm);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "307";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 增加客户

        #region 修改客户信息

        public static RetModel EditCustom(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                CustomModel custInfo = Comm.ResolveClass<CustomModel>(dc, "custInfo");
                CustomChecker checker = new CustomChecker { CustInfo = custInfo };
                checker.CheckData();
                checker.CustId = custInfo.Id;
                checker.CheckID();
                CustomSrv.EditCust(custInfo);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "308";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 修改客户信息

        #region 启用客户

        public static RetModel EnableCustom(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int custId = Comm.ResolveParam<int>(dc, "custId", 0);
                CustomChecker checker = new CustomChecker { CustId = custId };
                checker.CheckID();
                CustomSrv.EnableCust(custId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "309";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 启用客户

        #region 禁用客户

        public static RetModel DisableCustom(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int custId = Comm.ResolveParam<int>(dc, "custId", 0);
                CustomChecker checker = new CustomChecker { CustId = custId };
                checker.CheckID();
                CustomSrv.DisableCust(custId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "310";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 禁用客户

        #region 读客户信息

        public static RetModel GetCustInfo(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int custId = Comm.ResolveParam<int>(dc, "custId", 0);
                string custCode = Comm.ResolveParam<string>(dc, "custCode", "");
                CustomChecker checker = new CustomChecker { CustCode = custCode, CustId = custId };
                checker.CheckIdAndCode();
                rm.Data = custId > 0 ? CustomSrv.GetCustInfo(custId) : CustomSrv.GetCustInfo(custCode);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "311";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读客户信息

        #region 读客户列表

        public static RetModel GetCustList(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int cateId = Comm.ResolveParam<int>(dc, "cateId", 0);
                byte state = Comm.ResolveParam<byte>(dc, "state", 0);
                rm.Data = CustomSrv.GetCustList(cateId, state);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "312";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读客户列表

        #region 更新客户汇总数

        public static RetModel UpdateCustSummary(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int custId = Comm.ResolveParam<int>(dc, "custId", 0);
                CustomChecker checker = new CustomChecker { CustId = custId };
                checker.CheckID();
                CustomSrv.UpdateCustSummary(custId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "313";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 更新客户汇总数
    }
}