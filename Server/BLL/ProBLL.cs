﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Server.DAL;
using Server.Common;
using Server.Checkers;

namespace Server.BLL
{
    public static class ProBLL
    {
        #region 增加产品

        public static RetModel AddPro(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                ProductModel proInfo = Comm.ResolveClass<ProductModel>(dc, "proInfo");
                ProChecker checker = new ProChecker { ProInfo = proInfo };
                checker.CheckInfo();
                ProSrv.AddProduct(proInfo);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "401";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 增加产品

        #region 修改产品

        public static RetModel EditPro(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                ProductModel proInfo = Comm.ResolveClass<ProductModel>(dc, "proInfo");
                ProChecker checker = new ProChecker { ProInfo = proInfo };
                checker.CheckInfo();
                checker.ProId = proInfo.Id;
                checker.CheckID();
                ProSrv.EditProduct(proInfo);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "407";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 修改产品

        #region 更新单价

        public static RetModel UpdatePrice(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int proId = Comm.ResolveParam<int>(dc, "proId", 0);
                decimal price = Comm.ResolveParam<decimal>(dc, "price", 0);
                ProChecker checker = new ProChecker { ProId = proId };
                checker.CheckID();
                ProSrv.UpdatePrice(proId, price);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "406";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 更新单价

        #region 禁用产品

        public static RetModel DisablePro(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int proId = Comm.ResolveParam<int>(dc, "proId", 0);
                ProChecker checker = new ProChecker { ProId = proId };
                checker.CheckID();
                ProSrv.DisablePro(proId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "402";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 禁用产品

        #region 启用产品

        public static RetModel EnablePro(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int proId = Comm.ResolveParam<int>(dc, "proId", 0);
                ProChecker checker = new ProChecker { ProId = proId };
                checker.CheckID();
                ProSrv.EnablePro(proId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "402";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 启用产品

        #region 读产品信息

        public static RetModel GetProInfo(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int proId = Comm.ResolveParam<int>(dc, "proId", 0);
                string proCode = Comm.ResolveParam<string>(dc, "proCode", "");
                ProChecker checker = new ProChecker { ProCode = proCode, ProId = proId };
                checker.CheckIdAndCode();
                rm.Data = proId > 0 ? ProSrv.GetProInfo(proId) : ProSrv.GetProInfo(proCode);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "404";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读产品信息

        #region 读产品列表

        public static RetModel GetProList(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                string proCode = Comm.ResolveParam<string>(dc, "proCode", "");
                string proName = Comm.ResolveParam<string>(dc, "proName", "");
                byte state = Comm.ResolveParam<byte>(dc, "state", 255);
                rm.Data = ProSrv.GetProList(proCode, proName, state);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "405";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读产品列表
    }
}