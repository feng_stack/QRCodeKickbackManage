﻿using System;
using System.Collections.Generic;

using Models;

using Server.Checkers;
using Server.Common;
using Server.DAL;

namespace Server.BLL
{
    public static class CodeBLL
    {
        #region 增加编码

        public static RetModel AddCode(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                CodeModel codeInfo = Comm.ResolveClass<CodeModel>(dc, "codeInfo");
                CodeChecker checker = new CodeChecker
                {
                    CodeInfo = codeInfo
                };
                checker.CheckCode();
                CodeSrv.AddCode(codeInfo);
            }
            catch (Exception ex)
            {
                rm.ErrCode = "100";
                rm.ErrInfo = ex.Message;
                rm.Success = false;
            }
            return rm;
        }

        #endregion 增加编码

        #region 批量增加编码

        public static RetModel AddCodeBatch(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                List<CodeModel> codeList = Comm.ResolveClass<List<CodeModel>>(dc, "codeList");
                SessionModel sess = dc["sess"] as SessionModel;
                CodeChecker checker = new CodeChecker { CodeList = codeList };
                checker.CheckCodeList();
                for (int i = 0; i < codeList.Count; i++) { codeList[i].OperatorId = sess.UserId; }
                CodeSrv.AddCodeBatch(codeList);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "101";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 批量增加编码

        #region 作废编码

        public static RetModel CancelCode(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int codeId = Comm.ResolveParam<int>(dc, "codeId", 0);
                CodeChecker checker = new CodeChecker { CodeId = codeId };
                checker.CheckCode();
                CodeSrv.CancelCode(codeId);
            }
            catch (Exception ex)
            {
                rm.ErrCode = "102";
                rm.ErrInfo = ex.Message;
                rm.Success = false;
            }
            return rm;
        }

        #endregion 作废编码

        #region 读取编码信息

        public static RetModel GetCodeInfo(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int codeId = Comm.ResolveParam<int>(dc, "codeId", 0);
                string codeFull = Comm.ResolveParam<string>(dc, "codeFull", "");
                CodeChecker checker = new CodeChecker
                {
                    CodeFull = codeFull,
                    CodeId = codeId
                };
                checker.CheckCodeAndId();
                if (codeId > 0)
                    rm.Data = CodeSrv.GetCodeInfo(codeId);
                else
                    rm.Data = CodeSrv.GetCodeInfo(codeFull);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "103";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读取编码信息

        #region 读取编码列表

        public static RetModel GetCodeList(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                string year = Comm.ResolveParam<string>(dc, "year", "");
                string suffix = Comm.ResolveParam<string>(dc, "suffix", "");
                rm.Data = CodeSrv.GetCodeList(year, suffix);
            }
            catch (Exception ex)
            {
                rm.ErrCode = "104";
                rm.ErrInfo = ex.Message;
                rm.Success = false;
            }
            return rm;
        }

        #endregion 读取编码列表

        #region 读编码状态信息

        public static RetModel GetCodeStatus(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                string codeFull = Comm.ResolveParam<string>(dc, "codeFull", "");
                int codeId = Comm.ResolveParam<int>(dc, "codeId", 0);
                CodeChecker checker = new CodeChecker
                {
                    CodeId = codeId,
                    CodeFull = codeFull
                };
                checker.CheckCodeAndId();
                rm.Data = codeId > 0 ? CodeSrv.GetCodeStatusInfo(codeId) : CodeSrv.GetCodeStatusInfo(codeFull);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "105";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读编码状态信息

        #region 读取编码状态列表

        public static RetModel GetStatusList(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                string sDate = Comm.ResolveParam<DateTime>(dc, "sDate", DateTime.Now).ToString("yyyy-MM-dd 00:00:00");
                string eDate = Comm.ResolveParam<DateTime>(dc, "eDate", DateTime.Now).ToString("yyyy-MM-dd 23:59:59");
                int proId = Comm.ResolveParam<int>(dc, "proId", 0);
                byte state = Comm.ResolveParam<byte>(dc, "state", 255);
                rm.Data = CodeSrv.GetStatusList(sDate, eDate, proId, state);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "106";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读取编码状态列表
    }
}