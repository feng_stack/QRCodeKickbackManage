﻿using System;
using System.Collections.Generic;

using Models;

using Server.Checkers;
using Server.Common;
using Server.DAL;

namespace Server.BLL
{
    public static class BillBLL
    {
        #region 读编码信息

        public static RetModel GetCodeInfo(Dictionary<string, object> dc)
        {
            string fullCode = Comm.ResolveParam<string>(dc, "codeFull", "");
            return new RetModel { Data = !string.IsNullOrEmpty(fullCode) ? BillSrv.GetCodeInfo(dc["codeFull"].ToString()) : null, Success = true };
        }

        #endregion 读编码信息

        #region 增加单据

        public static RetModel AddSaleBill(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                CustomModel custInfo = Comm.ResolveClass<CustomModel>(dc, "custInfo");
                List<BillEntryModel> billEntry = Comm.ResolveClass<List<BillEntryModel>>(dc, "entry");
                SaleBillChecker checker = new SaleBillChecker
                {
                    Cust = custInfo,
                    Entry = billEntry
                };
                checker.CheckCustom();
                checker.CheckBillEntry();
                BillSrv.AddSaleBill(custInfo, billEntry);
                CustomSrv.UpdateCustSummary(custInfo.Id);
            }
            catch (Exception ex)
            {
                rm.ErrCode = "0001";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 增加单据

        #region 增加核销单

        public static RetModel AddChargeBill(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                CustomModel cm = Comm.ResolveClass<CustomModel>(dc, "custInfo");
                List<BillEntryModel> entry = Comm.ResolveClass<List<BillEntryModel>>(dc, "entry");
                ChargeBillChecker checker = new ChargeBillChecker
                {
                    Entry = entry,
                    Cust = cm
                };
                checker.CheckCustom();
                checker.CheckEntries();
                BillSrv.AddChargeBill(cm, entry);
                CustomSrv.UpdateCustSummary(cm.Id);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "002";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 增加核销单

        #region 读取商品汇总表

        public static RetModel GetProSummary(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                string sDate = Comm.ResolveParam<DateTime>(dc, "sDate", DateTime.Now).ToString("yyyy-MM-dd 00:00:00");
                string eDate = Comm.ResolveParam<DateTime>(dc, "eDate", DateTime.Now).ToString("yyyy-MM-dd 23:59:59");
                int proId = Comm.ResolveParam<int>(dc, "proId", 0);
                rm.Data = BillSrv.GetProSummary(sDate, eDate, proId);
            }
            catch (Exception ex)
            {
                rm.ErrCode = "003";
                rm.ErrInfo = ex.Message;
                rm.Success = false;
            }
            return rm;
        }

        #endregion 读取商品汇总表

        #region 读取核销汇总表

        public static RetModel GetChargeSummary(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                string sDate = Comm.ResolveParam<DateTime>(dc, "sDate", DateTime.Now).ToString("yyyy-MM-dd 00:00:00");
                string eDate = Comm.ResolveParam<DateTime>(dc, "eDate", DateTime.Now).ToString("yyyy-MM-dd 23:59:59");
                int proId = Comm.ResolveParam<int>(dc, "proId", 0);
                int cateId = Comm.ResolveParam<int>(dc, "cateId", 0);
                int custId = Comm.ResolveParam<int>(dc, "custId", 0);
                rm.Data = BillSrv.GetChargeSummary(sDate, eDate, proId, cateId, custId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "004";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读取核销汇总表

        #region 读取销售汇总表

        public static RetModel GetSaleSummary(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                string sDate = Comm.ResolveParam<DateTime>(dc, "sDate", DateTime.Now).ToString("yyyy-MM-dd 00:00:00");
                string eDate = Comm.ResolveParam<DateTime>(dc, "eDate", DateTime.Now).ToString("yyyy-MM-dd 23:59:59");
                int proId = Comm.ResolveParam<int>(dc, "proId", 0);
                int cateId = Comm.ResolveParam<int>(dc, "cateId", 0);
                int custId = Comm.ResolveParam<int>(dc, "custId", 0);
                rm.Data = BillSrv.GetSaleDetail(sDate, eDate, proId, cateId, custId);
                rm.Data = BillSrv.GetSaleSummary(sDate, eDate, cateId, custId, proId);
            }
            catch (Exception ex)
            {
                rm.ErrCode = "005";
                rm.ErrInfo = ex.Message;
                rm.Success = false;
            }
            return rm;
        }

        #endregion 读取销售汇总表

        #region 读取销售明细表

        public static RetModel GetSaleDetail(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                string sDate = Comm.ResolveParam<DateTime>(dc, "sDate", DateTime.Now).ToString("yyyy-MM-dd 00:00:00");
                string eDate = Comm.ResolveParam<DateTime>(dc, "eDate", DateTime.Now).ToString("yyyy-MM-dd 23:59:59");
                int proId = Comm.ResolveParam<int>(dc, "proId", 0);
                int cateId = Comm.ResolveParam<int>(dc, "cateId", 0);
                int custId = Comm.ResolveParam<int>(dc, "custId", 0);
                rm.Data = BillSrv.GetSaleDetail(sDate, eDate, proId, cateId, custId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "006";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读取销售明细表

        #region 读取核销明细表

        public static RetModel GetChargeDetail(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                string sDate = Comm.ResolveParam<DateTime>(dc, "sDate", DateTime.Now).ToString("yyyy-MM-dd 00:00:00");
                string eDate = Comm.ResolveParam<DateTime>(dc, "eDate", DateTime.Now).ToString("yyyy-MM-dd 23:59:59");
                int proId = Comm.ResolveParam<int>(dc, "proId", 0);
                int cateId = Comm.ResolveParam<int>(dc, "cateId", 0);
                int custId = Comm.ResolveParam<int>(dc, "custId", 0);
                rm.Data = BillSrv.GetChargeDetail(sDate, eDate, proId, cateId, custId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "007";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读取核销明细表

        #region 读取积分单列表

        public static RetModel GetPointBillList(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel() { Success = true };
            try
            {
                string sDate = Comm.ResolveParam<string>(dc, "sDate", "");
                string eDate = Comm.ResolveParam<string>(dc, "eDate", "");
                if (string.IsNullOrEmpty(sDate) || string.IsNullOrEmpty(eDate))
                    throw new Exception("时间段必须选择");
                sDate = Convert.ToDateTime(sDate).ToString("yyyy-MM-dd 00:00:00");
                eDate = Convert.ToDateTime(eDate).ToString("yyyy-MM-dd 23:59:59");
                string custName = Comm.ResolveParam<string>(dc, "custName", "");
                int maker = Comm.ResolveParam<int>(dc, "maker", 0);
                List<PointBillModel> l = BillSrv.GetPointBillList(sDate, eDate, custName, maker);
                rm.Data = l;
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "P001";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读取积分单列表

        #region 读积分单

        public static RetModel GetPointBill(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                int bId = Comm.ResolveParam<int>(dc, "bId", 0);
                if (bId <= 0)
                    throw new Exception("必须指定要查找的单据");
                PointBillModel pm = BillSrv.GetPointBill(bId);
                rm.Data = pm;
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "P002";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 读积分单

        #region 保存积分单

        public static RetModel SavePointBill(Dictionary<string, object> dc)
        {
            RetModel rm = new RetModel { Success = true };
            try
            {
                PointBillModel pm = Comm.ResolveClass<PointBillModel>(dc, "pm");
                if (pm is null)
                    throw new Exception("数据不能为空！");
                if (pm.CustId <= 0)
                    throw new Exception("客户必须选择");
                CustomModel cm = CustomSrv.GetCustInfo(pm.CustId);
                if (cm is null)
                    throw new Exception("指定的客户不存在!");
                if ((cm.CustTotalPoints - cm.CustPoints) < pm.UsePoint)
                    throw new Exception("客户积分不足");
                //保存
                BillSrv.SavePointBill(pm);
                //更新用户积分
                CustomSrv.UpdateCustSummary(pm.CustId);
            }
            catch (Exception ex)
            {
                rm.Success = false;
                rm.ErrCode = "P003";
                rm.ErrInfo = ex.Message;
            }
            return rm;
        }

        #endregion 保存积分单
    }
}