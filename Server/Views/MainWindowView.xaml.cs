﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Server.Views
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindowView : Window
    {
        public MainWindowView()
        {
            InitializeComponent();
            InitMenu();
            InitTrayIcon();
        }

        private bool running = true;
        private System.Windows.Forms.NotifyIcon notify = new System.Windows.Forms.NotifyIcon();
        private System.Windows.Forms.ContextMenu menu;

        private void Notify_DoubleClick(object sender, EventArgs e)
        {
            if (Visibility == Visibility.Visible)
                Visibility = Visibility.Hidden;
            else
                Visibility = Visibility.Visible;
        }

        private void InitMenu()
        {
            menu = new System.Windows.Forms.ContextMenu();
            System.Windows.Forms.MenuItem m = new System.Windows.Forms.MenuItem("退出");
            m.Click += M_Click;
            menu.MenuItems.Add(m);
        }

        private void M_Click(object sender, EventArgs e)
        {
            running = false;
            Close();
        }

        private void InitTrayIcon()
        {
            notify.Icon = Properties.Resources.Skull;
            notify.Visible = true;
            notify.Text = "Gift Manage Server";
            notify.DoubleClick += Notify_DoubleClick;
            notify.ContextMenu = menu;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (running)
            {
                e.Cancel = true;
                Visibility = Visibility.Hidden;
            }
            else
            {
                notify.Visible = false;
            }
        }
    }
}