﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data;
using System.Data.SQLite;
using Server.Common;

namespace Server.DAL
{
    public static class ProSrv
    {
        #region 转换

        private static ProductModel DrToPro(DataRow dr)
        {
            if (dr is null)
                return null;
            ProductModel pm = new ProductModel
            {
                DefaultPrice = Comm.Cdr<decimal>(dr, "default_price"),
                Id = Comm.Cdr<int>(dr, "id"),
                ProCode = Comm.Cdr<string>(dr, "pro_code"),
                ProModel = Comm.Cdr<string>(dr, "pro_model"),
                ProName = Comm.Cdr<string>(dr, "pro_name"),
                Remark = Comm.Cdr<string>(dr, "pro_remark"),
                ProState = Comm.Cdr<byte>(dr, "pro_state")
            };
            return pm;
        }

        #endregion 转换

        #region 增加一个商品

        public static void AddProduct(ProductModel p)
        {
            try
            {
                string sqlstr = "insert into t_product(pro_code,pro_name,pro_model,pro_remark,default_price) values(@pro_code,@pro_name,@pro_model,@pro_remark,@default_price)";
                SQLiteParameter[] op = new SQLiteParameter[]
                {
                    new SQLiteParameter{DbType=DbType.String,ParameterName="@pro_code",Value=p.ProCode},
                    new SQLiteParameter {DbType=DbType.String,ParameterName="@pro_name",Value=p.ProName },
                    new SQLiteParameter {DbType=DbType.String,ParameterName="@pro_model",Value=p.ProModel },
                    new SQLiteParameter {DbType=DbType.String,ParameterName="@pro_remark",Value=p.Remark },
                    new SQLiteParameter {DbType=DbType.Decimal,ParameterName="@default_price",Value=p.DefaultPrice}
                };
                Comm.DB.Exec(sqlstr, op);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion 增加一个商品

        #region 读取单个商品信息

        public static ProductModel GetProInfo(int proId)
        {
            string sqlstr = "select * from t_product where id=" + proId;
            var dt = Comm.DB.Query(sqlstr);
            if (null == dt)
                throw new Exception("未知错误");
            if (dt.Rows.Count < 1)
                return null;
            return DrToPro(dt.Rows[0]);
        }

        public static ProductModel GetProInfo(string proCode)
        {
            string sqlstr = $"select * from t_product where pro_code='{proCode}'";
            var dt = Comm.DB.Query(sqlstr);
            if (null == dt)
                throw new Exception("未知错误！");
            if (dt.Rows.Count < 1)
                return null;
            return DrToPro(dt.Rows[0]);
        }

        #endregion 读取单个商品信息

        #region 读产品列表

        public static List<ProductModel> GetProList(string proCode = "", string proName = "", byte proState = 255)
        {
            string sqlstr = $"select * from t_product where 1=1";
            if (proState != 255)
                sqlstr += " and pro_state=" + proState;
            if (!string.IsNullOrWhiteSpace(proCode))
                sqlstr += $" and pro_code like '%{proCode}%'";
            if (!string.IsNullOrWhiteSpace(proName))
                sqlstr += $" and pro_name like '%{proName}%'";
            var dt = Comm.DB.Query(sqlstr);
            if (null == dt)
                throw new Exception("未知错误");
            if (dt.Rows.Count < 1)
                return null;
            List<ProductModel> l = new List<ProductModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dr = dt.Rows[i];
                l.Add(DrToPro(dr));
            }
            return l;
        }

        #endregion 读产品列表

        #region 启用/禁用

        public static void DisablePro(int proId)
        {
            string sqlstr = "update t_product set pro_state=1 where id=" + proId;
            Comm.DB.Query(sqlstr);
        }

        public static void EnablePro(int proId)
        {
            string sqlstr = "update t_product set pro_state=0 where id=" + proId;
            Comm.DB.Query(sqlstr);
        }

        #endregion 启用/禁用

        #region 更新兑换价

        public static void UpdatePrice(int proId, decimal price)
        {
            string sqlstr = $"update t_product set default_price={price} where id={proId}";
            Comm.DB.Exec(sqlstr);
        }

        #endregion 更新兑换价

        #region 修改产品

        public static void EditProduct(ProductModel pro)
        {
            string sqlstr = "update t_product set pro_code=@pro_code,pro_name=@pro_name,pro_model=@pro_model,pro_remark=@pro_remark,default_price=@default_price where id=@id";
            SQLiteParameter[] op = new SQLiteParameter[]
            {
                new SQLiteParameter{DbType=DbType.String,ParameterName="@pro_code",Value=pro.ProCode},
                new SQLiteParameter {DbType=DbType.String,ParameterName="@pro_name",Value=pro.ProName},
                new SQLiteParameter {DbType=DbType.String,ParameterName="@pro_model",Value=pro.ProModel},
                new SQLiteParameter {DbType=DbType.String,ParameterName="@pro_remark",Value=pro.Remark},
                new SQLiteParameter {DbType=DbType.Decimal,ParameterName="@default_price",Value=pro.DefaultPrice },
                new SQLiteParameter {DbType=DbType.Int32,ParameterName="@id",Value=pro.Id}
            };
            Comm.DB.Exec(sqlstr, op);
        }

        #endregion 修改产品
    }
}