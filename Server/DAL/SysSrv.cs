﻿using Server.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.DAL
{
    public static class SysSrv
    {
        public static bool CheckVerTable()
        {
            string sqlstr = "select * from sqlite_master where type='table' and name='t_version'";
            var dt = Comm.DB.Query(sqlstr);
            if (null == dt)
                return false;
            if (dt.Rows.Count < 1)
                return false;
            return true;
        }

        public static void CreateVersionTable()
        {
            string sqlstr = "CREATE TABLE t_version (version TEXT NOT NULL); ";
            Comm.DB.Exec(sqlstr);
        }

        public static string GetDBVer()
        {
            string sqlstr = "select version from t_version";
            var dt = Comm.DB.Query(sqlstr);
            if (dt is null)
                return "";
            if (dt.Rows.Count < 1)
                return "";
            return dt.Rows[0][0].ToString();
        }

        public static void UpdateDBVer(string sqlstr)
        {
            Comm.DB.Exec(sqlstr);
        }
    }
}