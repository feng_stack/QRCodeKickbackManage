﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data;
using System.Data.SQLite;
using Server.Common;

namespace Server.DAL
{
    public static class UserSrv
    {
        #region 模型转换
        private static UserModel Dr2User(DataRow dr)
        {
            if (dr is null)
                return null;
            try
            {
                UserModel um = new UserModel
                {
                    Id = Comm.Cdr<int>(dr, "id"),
                    Password = Comm.Cdr<string>(dr, "password"),
                    RealName = Comm.Cdr<string>(dr, "real_name"),
                    UserName = Comm.Cdr<string>(dr, "user_name"),
                    Role = Comm.Cdr<byte>(dr, "role"),
                    LastLogin=Comm.Cdr<string>(dr,"last_login"),
                    State=Comm.Cdr<byte>(dr,"user_state")
                };
                return um;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
        #region 读用户信息
        public static UserModel GetUserInfo(string userName)
        {
                if (string.IsNullOrEmpty(userName))
                    throw new Exception("用户名不能为空！");
                string sqlstr = $"select * from t_user where user_name='{userName}'";
                var dt = Comm.DB.Query(sqlstr);
                if (null == dt)
                    throw new Exception("未知错误");
                if (dt.Rows.Count < 1)
                    return null;
                return Dr2User(dt.Rows[0]);            
        }
        public static UserModel GetUserInfo(int uId)
        {
            string sqlstr = $"select * from t_user where id={uId}";
            var dt = Comm.DB.Query(sqlstr);
            if (dt.Rows.Count < 1)
                return null;
            return (Dr2User(dt.Rows[0]));
        }
        #endregion
        #region 读用户列表
        public static List<UserModel> GetUserList(byte state = 0)
        {
            try
            {
                string sqlstr = "select * from t_user where 1=1";
                if (state == 0)
                    sqlstr += " and user_state=0";
                if (state == 1)
                    sqlstr += " and user_state=1";
                var dt = Comm.DB.Query(sqlstr);
                if (null == dt)
                    throw new Exception("未知错误！");
                if (dt.Rows.Count < 1)
                    return null;
                List<UserModel> lst = new List<UserModel>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var dr = dt.Rows[i];
                    lst.Add(Dr2User(dr));
                }
                return lst;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
        #region 增加用户
        public static void AddUser(UserModel um)
        {
                string sqlstr = "insert into t_user(user_name,password,real_name,role) values(@user_name,@password,@real_name,@role)";
                SQLiteParameter[] op = new SQLiteParameter[]
                {
                    new SQLiteParameter {DbType=DbType.String,ParameterName="@user_name",Value=um.UserName},
                    new SQLiteParameter {DbType=DbType.String,ParameterName="@password",Value=um.Password},
                    new SQLiteParameter {DbType=DbType.String,ParameterName="@real_name",Value=um.RealName},
                    new SQLiteParameter {DbType=DbType.Boolean,ParameterName="@role",Value=um.Role}
                };
                Comm.DB.Exec(sqlstr,op);
            
        }
        #endregion
        #region 修改密码
        public static void EditPass(string nPass,int uId)
        {
            string sqlstr = $"update t_user set password='{nPass}' where id={uId}";
            Comm.DB.Exec(sqlstr);
        }
        #endregion
        #region 修改用户信息
        public static void EditUser(UserModel um)
        {
            string sqlstr = $"update t_user set user_name='{um.UserName}',real_name='{um.RealName}',role={um.Role} where id={um.Id}";
            Comm.DB.Exec(sqlstr);
        }
        #endregion
        #region 修改用户状态
        public static void EnableUser(int uId)
        {
            string sqlstr = $"update t_user set user_state=0 where id={uId}";
            Comm.DB.Exec(sqlstr);
        }
        public static void DisableUser(int uId)
        {
            string sqlstr = $"update t_user set user_state=1 where id={uId}";
            Comm.DB.Exec(sqlstr);
        }
        #endregion
        #region 更新用户最后登录日期
        public static void UpdateLoginTime(int uId)
        {
            string sqlstr = "update t_user set last_login=datetime('now','localtime') where id=" + uId;
            Comm.DB.Exec(sqlstr);
        }
        #endregion
    }
}
