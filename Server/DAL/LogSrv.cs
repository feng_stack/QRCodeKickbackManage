﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using Models;
using Server.Common;

namespace Server.DAL
{
    public static class LogSrv
    {
        #region 转换
        private static UserLogModel DrToLog(DataRow dr)
        {
            if (null == dr)
                return null;
            UserLogModel ulm = new UserLogModel
            {
                Id = Comm.Cdr<int>(dr, "id") ,
                LogDate = Comm.Cdr<string>(dr, "log_date") ,
                OperateResult = Comm.Cdr<string>(dr, "log_result") ,
                Operation = Comm.Cdr<string>(dr, "log_operate") ,
                UserId = Comm.Cdr<int>(dr, "user_id"),
                UserName = Comm.Cdr<string>(dr, "user_name")
            };
            return ulm;
        }
        #endregion
        #region 写日志到数据库
        public static void Write(int uId,string operate,string result="")
        {
            string sqlstr = $"insert into t_user_log(user_id,log_operate,log_result) values({uId},'{operate}','{result}')";
            Comm.DB.Exec(sqlstr);
        }
        #endregion
        #region 读单条日志
        public static UserLogModel GetLog(int logId)
        {
            string sqlstr = $"select t.*,t1.user_name from t_user_log t left join t_user t1 on t.user_id = t1.id where t.id={logId}";
            var dt = Comm.DB.Query(sqlstr);
            if (null == dt)
                throw new Exception("未知错误！");
            if (dt.Rows.Count < 1)
                return null;
            return DrToLog(dt.Rows[0]);
        }
        #endregion
        #region 读取日志列表
        public static List<UserLogModel> GetLogList(int uId = 0,string sDate="",string eDate="",string operate="")
        {
            string sqlstr = "select t.*,t1.user_name from t_user_log t left join t_user t1 on t.user_id = t1.id";
            List<string> l = new List<string>();
            if (uId > 0)
                l.Add($"t.user_id={uId}");
            if(!string.IsNullOrWhiteSpace(sDate) && !string.IsNullOrWhiteSpace(eDate))
            {
                sDate = Convert.ToDateTime(sDate).ToString("yyyy-MM-dd 00:00:00");
                eDate = Convert.ToDateTime(eDate).ToString("yyyy-MM-dd 23:59:59");
                l.Add($"t.log_date>='{sDate}' and t.log_date<='{eDate}'");
            }
            if (!string.IsNullOrWhiteSpace(operate))
                l.Add($"t.log_operate='{operate}'");
            if (l.Count > 0)
                sqlstr += " where ";
            for (int i = 0; i < l.Count; i++)
            {
                if (i == 0)
                    sqlstr += l[i];
                else
                    sqlstr += " and " + l[i];
            }
            var dt = Comm.DB.Query(sqlstr);
            if(null==dt)
                throw new Exception("未知错误");
            if (dt.Rows.Count < 1)
                return null;
            List<UserLogModel> lst = new List<UserLogModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dr = dt.Rows[i];
                lst.Add(DrToLog(dr));
            }
            return lst;
        }
        #endregion
        #region 删除日志
        public static void ClearLog(int uId=0,string sDate="",string eDate="",string operate="")
        {
            string sqlstr = $"delete from t_user_log";
            List<string> l = new List<string>();
            if (uId > 0)
                l.Add($"user_id={uId}");
            if(!string.IsNullOrWhiteSpace(sDate) && !string.IsNullOrWhiteSpace(eDate))
            {
                sDate = Convert.ToDateTime(sDate).ToString("yyyy-MM-dd 00:00:00");
                eDate = Convert.ToDateTime(eDate).ToString("yyyy-MM-dd 23:59:59");
                l.Add($"log_date>='{sDate}' and log_date<='{eDate}'");
            }
            if (!string.IsNullOrWhiteSpace(operate))
                l.Add($"log_operate='{operate}'");
            if (l.Count > 0)
                sqlstr += " where ";
            for (int i = 0; i < l.Count; i++)
            {
                if (i == 0)
                    sqlstr += l[i];
                else
                    sqlstr += " and " + l[i];
            }
            Comm.DB.Exec(sqlstr);
        }
        #endregion
    }
}
