﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Prism.Unity;
using Prism.Mvvm;
using Prism.Ioc;
using Server.Common;

namespace Server
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : PrismApplication
    {
        protected override Window CreateShell()
        {
            Comm.Cfg.ReadConfig(AppDomain.CurrentDomain.BaseDirectory + @"\config.xml");
            var shell = Container.Resolve<Views.MainWindowView>();
            Comm.Shell = shell;
            shell.Closing += (s, e) => { Comm.RaiseMainFormClosing(s, e); };
            return shell;
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}