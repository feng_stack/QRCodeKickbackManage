PRAGMA foreign_keys = 0;

CREATE TABLE sqlitestudio_temp_table AS SELECT *
                                          FROM t_code_status;

DROP TABLE t_code_status;

CREATE TABLE t_code_status (
    code_id       INTEGER  NOT NULL,
    sale_flag     BOOLEAN  NOT NULL
                           DEFAULT (0),
    sale_custom   INTEGER  NOT NULL
                           DEFAULT (0),
    sale_date     DATETIME,
    sale_maker    INTEGER  NOT NULL
                           DEFAULT (0),
    charge_flag   BOOLEAN  NOT NULL
                           DEFAULT (0),
    charge_custom INTEGER  NOT NULL
                           DEFAULT (0),
    charge_date   DATETIME,
    charge_maker  INTEGER  NOT NULL
                           DEFAULT (0),
    charge_amount DECIMAL  NOT NULL
                           DEFAULT (0),
    charge_point  INTEGER  NOT NULL
                           DEFAULT (0) 
);

INSERT INTO t_code_status (
                              code_id,
                              sale_flag,
                              sale_custom,
                              sale_date,
                              sale_maker,
                              charge_flag,
                              charge_custom,
                              charge_date,
                              charge_maker,
                              charge_amount
                          )
                          SELECT code_id,
                                 sale_flag,
                                 sale_custom,
                                 sale_date,
                                 sale_maker,
                                 charge_flag,
                                 charge_custom,
                                 charge_date,
                                 charge_maker,
                                 charge_amount
                            FROM sqlitestudio_temp_table;

DROP TABLE sqlitestudio_temp_table;

CREATE INDEX Index_code_id ON t_code_status (
    code_id ASC
);

PRAGMA foreign_keys = 1;


CREATE TABLE t_pointbill (
    id        INTEGER  PRIMARY KEY
                       NOT NULL,
    cust_id   INTEGER  NOT NULL,
    use_date  DATETIME NOT NULL
                       DEFAULT (datetime('now', 'localtime') ),
    use_point INTEGER  NOT NULL
                       DEFAULT (0),
    remark    TEXT,
    maker     INTEGER  NOT NULL
);

PRAGMA foreign_keys = 0;

CREATE TABLE sqlitestudio_temp_table AS SELECT *
                                          FROM t_custom;

DROP TABLE t_custom;

CREATE TABLE t_custom (
    id              INTEGER PRIMARY KEY AUTOINCREMENT,
    cate_id         INTEGER NOT NULL,
    c_code          TEXT    NOT NULL,
    c_name          TEXT    NOT NULL,
    c_phone         TEXT,
    c_remark        TEXT,
    c_charge_count  DECIMAL NOT NULL
                            DEFAULT (0),
    c_charge_amount DECIMAL NOT NULL
                            DEFAULT (0),
    c_state         BOOLEAN NOT NULL
                            DEFAULT (0),
    c_points        INTEGER NOT NULL
                            DEFAULT (0),
    c_total_points  INTEGER NOT NULL
                            DEFAULT (0) 
);

INSERT INTO t_custom (
                         id,
                         cate_id,
                         c_code,
                         c_name,
                         c_phone,
                         c_remark,
                         c_charge_count,
                         c_charge_amount,
                         c_state
                     )
                     SELECT id,
                            cate_id,
                            c_code,
                            c_name,
                            c_phone,
                            c_remark,
                            c_charge_count,
                            c_charge_amount,
                            c_state
                       FROM sqlitestudio_temp_table;

DROP TABLE sqlitestudio_temp_table;

PRAGMA foreign_keys = 1;

